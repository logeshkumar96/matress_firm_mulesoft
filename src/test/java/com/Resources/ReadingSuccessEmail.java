package com.Resources;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.RecipientStringTerm;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.w3c.dom.NodeList;

import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.Utilities;
import com.google.common.io.Files;

public class ReadingSuccessEmail {

	public static void fetch(String pop3Host, String storeType, String user, String password) {
		String sub = null;

		try {
			// create properties field
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", pop3Host);
			properties.put("mail.pop3.port", "995");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);
			// emailSession.setDebug(true);

			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("pop3s");

			store.connect(pop3Host, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			// System.out.println("messages.length---" + messages.length);

			for (int i = messages.length; i >= messages.length - 15; i--) {
				// System.out.println();

				Message message = messages[i - 1];
				Address[] from = message.getFrom();
				Address ad = from[0];
				String address = ad.toString();

				if (address.equals(HashMapContainer.get("$$FROM_ADDRESS")) && sub == null) {
					sub = message.getSubject().toString();
					// System.out.println("---------------------------------");

					writePart(message);
					break;

				}
				/*
				 * String line = reader.readLine();
				 * 
				 * if ("YES".equals(line)) { message.writeTo(System.out); } else if
				 * ("QUIT".equals(line)) { break; }
				 */}
			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getemailRead() {
		String host = "outlook.office365.com";// change accordingly
		String mailStoreType = "pop3";
		String username = "logeshkumar.r@royalcyber.com";// change accordingly
		String password = "Logesh@5599";// change accordingly

		// Call method fetch
		fetch(host, mailStoreType, username, password);

	}

	/*
	 * This method checks for content-type based on which, it processes and fetches
	 * the content of the message
	 */
	public static void writePart(Part p) throws Exception {
		if (p instanceof Message)
			// Call methos writeEnvelope
			writeEnvelope((Message) p);

		System.out.println("----------------------------");
		System.out.println("CONTENT-TYPE: " + p.getContentType());

		// check if the content is plain text
		if (p.isMimeType("text/plain")) {
			System.out.println("This is plain text");
			System.out.println("---------------------------");
			System.out.println((String) p.getContent());
			String text = (String) p.getContent();
			Assert.assertTrue(text.contains("Test - Process Completed Successfully"));
		}
		// check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			System.out.println("This is a Multipart");
			System.out.println("---------------------------");
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++)
				writePart(mp.getBodyPart(i));
		}
		// check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {
			System.out.println("This is a Nested Message");
			System.out.println("---------------------------");
			writePart((Part) p.getContent());
		} else if (p.isMimeType("multipart/*")) {
			/*
			 * System.out.println("This is a Multipart");
			 * System.out.println("---------------------------");
			 */
			File f = new File(System.getProperty("user.dir") + "\\src\\test\\java\\BPMfiles\\Test.pdf");
			File f2 = new File(System.getProperty("user.dir") + "\\src\\test\\java\\BPMfiles\\Test.txt");

			String contentBody = null;

			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < 2; i++) {
				if (i == 1) {
					BodyPart bodyPart = mp.getBodyPart(i);
					bodyPart.writeTo(new FileOutputStream(f));

				} else {
					BodyPart bodyPart = mp.getBodyPart(i);
					Object content = bodyPart.getContent();
					String string = content.toString();

					org.jsoup.nodes.Document doc = Jsoup.parse(string);
					String o = doc.text();

					String wholeBody = contentBody + "\n" + o;
					FileWriter x = new FileWriter(f2);
					x.write(wholeBody);
					x.close();

				}
			}
		} else if (p.getContentType().contains("image/")) {
			System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				// System.out.println("This is a string");
				// System.out.println("---------------------------");
				String html = (String) o;

				File htmlFile = new File(
						System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\EmailBody.html");
				FileOutputStream fio = new FileOutputStream(htmlFile);
				fio.write(html.getBytes());
				fio.close();
				// System.out.println((String) o);
			} else if (o instanceof InputStream) {
				System.out.println("This is just an input stream");
				System.out.println("---------------------------");
				String f = "D:\\Child%20TEAF\\rc_teaf_child\\src\\test\\java\\BPMfiles\\";
				if (p.getContent() instanceof Multipart) {
					Multipart multipart = (Multipart) p.getContent();

					for (int i = 0; i < multipart.getCount(); i++) {
						Part part = multipart.getBodyPart(i);
						String disposition = part.getDisposition();
						System.out.println("====================================");
						if ((disposition != null) && ((disposition.equalsIgnoreCase(Part.ATTACHMENT)
								|| (disposition.equalsIgnoreCase(Part.INLINE))))) {
							MimeBodyPart mimeBodyPart = (MimeBodyPart) part;
							String fileName = mimeBodyPart.getFileName();
							System.out.println(fileName);
							File fileToSave = new File(f + fileName);
							mimeBodyPart.saveFile(fileToSave);
						}
					}
				}

			} else {
				System.out.println("This is an unknown type");
				System.out.println("---------------------------");
				System.out.println(o.toString());
			}
		}

	}

	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public static void writeEnvelope(Message m) throws Exception {
		// System.out.println("This is the message envelope");
		// System.out.println("---------------------------");
		Address[] a;

		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("FROM: " + a[j].toString());
		}

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("TO: " + a[j].toString());
		}

		// SUBJECT
		if (m.getSubject() != null)
			System.out.println("SUBJECT: " + m.getSubject());

		Date receivedDate = m.getSentDate();
		System.out.println("DATE: " + receivedDate);

	}
	

}
