package com.Resources;

public final class AdvancedCryptoConstants
{
   protected static final String CIPHER_ALGORITHM_MODE_AND_PADDING  = "AES/CTR/NoPadding";
   protected static final byte[] ENCRYPTION_KEY_DERIVATION_CONSTANT = { (byte) 0x00 };
   protected static final byte[] HMAC_KEY_DERIVATION_CONSTANT       = { (byte) 0x01 };
   protected static final int    IV_LENGTH                          = 16;
   protected static final int    MAC_TRUNCATED_SIZE                 = 16;

   private AdvancedCryptoConstants()
   {
   }
}