package com.Resources;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.IIOException;
import javax.security.sasl.AuthenticationException;

/**
 * This class is suitable for working with SDP data <b>outside</b> of an application framework that automatically handles URL encoding/decoding of data.
 *
 */
public class SDPProcessor
{

   /**
    * Takes a plain-text string and produces an encrypted and encoded (Base-64 then URL encoding) string. Performs, in order:
    * <ol>
    * <li>Encryption - as specified by the associated cryptographic implementation provided by the crypto parameter</li>
    * <li>Base 64 encoding</li>
    * <li>URL encoding</li>
    *
    * </ol>
    *
    * @param input
    *           - plain-text string to encrypt and encode
    * @param crypto
    *           - cryptographic algorithm
    * @return
 * @throws Throwable 
    * @throws InvalidDataException
    * @throws UnsupportedEncodingException
    */
   public static String encryptAndEncodeOutgoingPacket(String input, Crypto crypto) throws Throwable
   {
      byte[] data = input.getBytes(StandardCharsets.UTF_8);
      data = crypto.encrypt(data);
      return new String(Base64.getUrlEncoder().encode(data), StandardCharsets.UTF_8);
   }

   /**
    * Takes a URL encoded, Base-64 encoded, encrypted UTF-8 encoded string as input and returns a decrypted UTF-8 plain-text string. Performs (in order):
    * <ol>
    * <li>URL decoding</li>
    * <li>Base 64 decoding</li>
    * <li>Decryption - as specified by the associated cryptographic implementation provided by the crypto parameter</li>
    * </ol>
    *
    * @param input
    * @param crypto
    * @return
 * @throws Exception 
    * @throws UnsupportedEncodingException
    * @throws InvalidDataException
    */
   public static String decodeAndDecryptIncomingPacket(String input, Crypto crypto) throws Exception
   {
      if (input == null)
      {
         throw new Exception();
      }
      byte[] data = crypto.decrypt(Base64.getUrlDecoder().decode(input));
      return new String(data, StandardCharsets.UTF_8);
   }

   public static String decodeByteArray(byte[] input, Crypto crypto) throws Exception
   {
      if (input == null)
      {
         throw new Exception();
      }
      return new String(crypto.decrypt(input), StandardCharsets.UTF_8);
   }

   private SDPProcessor()
   {
   }
}