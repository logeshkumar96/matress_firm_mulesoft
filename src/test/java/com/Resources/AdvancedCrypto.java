package com.Resources;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.IIOException;
import javax.security.sasl.AuthenticationException;

/**
 * THIS IMPLEMENTATION IS NOT CURRENTLY AVAILABLE IN PRODUCTION
 *
 */
public class AdvancedCrypto implements Crypto
{
   private final SecretKey derivedEncryptionKey;
   private final SecretKey derivedHMACKey;

   public AdvancedCrypto(Key shared) throws NoSuchAlgorithmException, InvalidKeyException
   {
      Mac keyDerivator = Mac.getInstance("HmacSHA256");
      keyDerivator.init(shared);

      // Derive new keys:
      // Take the HMAC of the constants, using the master key
      byte[] data = keyDerivator.doFinal(AdvancedCryptoConstants.ENCRYPTION_KEY_DERIVATION_CONSTANT);
      derivedEncryptionKey = new SecretKeySpec(data, "AES");

      data = keyDerivator.doFinal(AdvancedCryptoConstants.HMAC_KEY_DERIVATION_CONSTANT);
      derivedHMACKey = new SecretKeySpec(data, "AES");
   }

   /**
    * Create encrypted message by:
    * <ol>
    * <li>Generate an IV</li>
    * <li>Encrypt data</li>
    * <li>Concatenate hmac + iv + encrypted data</li>
 * @throws Throwable 
    *
    */
   @Override
   public byte[] encrypt(byte[] data) throws Throwable
   {
      byte[] iv = new byte[AdvancedCryptoConstants.IV_LENGTH];
      new SecureRandom().nextBytes(iv);

      byte[] encryptedData = computeSymmetricEncryption(iv, data);

      byte[] mac = computeHMAC(iv, encryptedData);

      return concatenateByteArrays(mac, iv, encryptedData);
   }

   /**
    * Verify, then decrypt the data
 * @throws Exception 
    *
    * @throws InvalidDataException
    *
    */
   @Override
   public byte[] decrypt(byte[] data) throws Exception
   {
      if ((AdvancedCryptoConstants.IV_LENGTH + AdvancedCryptoConstants.MAC_TRUNCATED_SIZE) > data.length)
      {
         throw new Exception();
      }

      byte[] receivedHMAC = Arrays.copyOfRange(data, 0, AdvancedCryptoConstants.MAC_TRUNCATED_SIZE);
      byte[] receivedIV = Arrays.copyOfRange(data, AdvancedCryptoConstants.MAC_TRUNCATED_SIZE, AdvancedCryptoConstants.MAC_TRUNCATED_SIZE + AdvancedCryptoConstants.IV_LENGTH);
      byte[] receivedCipherText = Arrays.copyOfRange(data, AdvancedCryptoConstants.MAC_TRUNCATED_SIZE + AdvancedCryptoConstants.IV_LENGTH, data.length);

      // compute an expected HMAC given the received IV and encrypted data
      byte[] expectedHMAC = computeHMAC(receivedIV, receivedCipherText);

      // ensure that the received and calulated/expected HMAC are identical
      if (!areHMACEqual(receivedHMAC, expectedHMAC))
      {
         throw new AuthenticationException();
      }

      return computeSymmetricEncryption(receivedIV, receivedCipherText);
   }

   /**
    * AES encryption is symmetric, so only one encryption method is needed to encrypt or decrypt the data.
    *
    * @param iv
    *           - initial vector used to seed encryption mode
    *
    * @param cipherText
    *           - encrypted text to decrypt
    *
    * @return - decrypted byte array
    * @throws NoSuchAlgorithmException
    * @throws NoSuchPaddingException
    * @throws InvalidKeyException
    * @throws InvalidAlgorithmParameterException
    * @throws IllegalBlockSizeException
    * @throws BadPaddingException
    */
   private byte[] computeSymmetricEncryption(byte[] iv, byte[] cipherText)
         throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
   {
      Cipher encryptCipher = Cipher.getInstance("AES/CTR/NoPadding");
      encryptCipher.init(javax.crypto.Cipher.ENCRYPT_MODE, this.derivedEncryptionKey, new IvParameterSpec(iv));
      return encryptCipher.doFinal(cipherText);
   }

   /**
    * Constant time comparison of HMAC fields. Done in constant time to reduce the vulnerability of detecting where there is a difference in the expected vs. received codes
    *
    * @param expected
    * @param received
    * @return
    */
   private boolean areHMACEqual(byte[] expected, byte[] received)
   {
      if (expected.length != received.length)
      {
         return false;
      }

      boolean equalSoFar = true;

      for (int i = 0; i < expected.length; i++)
      {
         equalSoFar &= (expected[i] == received[i]);
      }

      return equalSoFar;
   }

   /**
    * Compute a SHA-256 HMAC value based on an initial value and cypher text
    *
    * @param iv
    * @param ct
    * @return
    * @throws NoSuchAlgorithmException
    * @throws InvalidKeyException
    */
   private byte[] computeHMAC(byte[] iv, byte[] ct) throws NoSuchAlgorithmException, InvalidKeyException
   {
      Mac hmac = Mac.getInstance("HmacSHA256");
      hmac.init(derivedHMACKey);
      hmac.update(iv);
      hmac.update(ct);

      // Keep only the first 16 bytes of the HMAC value
      return Arrays.copyOf(hmac.doFinal(), AdvancedCryptoConstants.MAC_TRUNCATED_SIZE);
   }
}