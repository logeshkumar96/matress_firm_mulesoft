package com.Resources;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.IIOException;
import javax.security.sasl.AuthenticationException;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.DecoderException;
import org.junit.BeforeClass;
import org.junit.Test;

import com.TEAF.framework.HashMapContainer;

public class SDPEncryptDecrypt {
	
	public static SecretKey decodeKeyString(String hexEncodedKey)
	   {
	      return new SecretKeySpec(DatatypeConverter.parseHexBinary(hexEncodedKey), "AES");
	   }
	
	public static String shouldGenerateSampleData(String param1, String param2,String param3, String param4,String param5, String param6,String param7, String param8,String param9, String param10,String param11, String param12,String param13, String param14,String param15, String param16,String param17, String param18,String param19)
	         throws Throwable
	   {
		   
	      SecretKey key = decodeKeyString("20CDDC17B13C0F9FA7D7F033F675A942809484B6654208295DC894D2AAC3F6C3");
	      Crypto crypto = new AdvancedCrypto(key);

	      String s1 = "0365="+param1;
	      String s2 = "|0038="+param2;
	      String s3 = "|0709="+param3;
	      String s4 = "|CVV="+param4;
	      String s5 = "|appKey="+param5;
	      String s6 = "|0009="+param6;
	      String s7 = "|0010="+param7;
	      String s8 = "|0012="+param8;
	      String s9 = "|0014="+param9;
	      String s10 = "|0015="+param10;
	      String s11 = "|0016="+param11;
	      String s12 = "|0017="+param12;
	      String s13 = "|0018="+param13;
	      String s14 = "|0569="+param14;
	      String s15 = "|postBackRefId="+param15;
	      String s16 = "|PhoneNumber="+param16;
	      String s17 = "|0116="+param17;
	      String s18 = "|siteCode="+param18;
	      String s19 = "|DT="+HashMapContainer.get("$$Timestamp");
	      String s = s1+s2+s3+s4+s5+s6+s7+s8+s9+s10+s11+s12+s13+s14+s15+s16+s17+s18+s19;
	      System.out.println(s);
	      String outbound = SDPProcessor.encryptAndEncodeOutgoingPacket(s, crypto);
	      System.out.println("Encrypted SDP data: "+outbound);
	      String inbound = SDPProcessor.decodeAndDecryptIncomingPacket(outbound, crypto);
	      assertThat(s, equalTo(inbound));
		return outbound;
	   }

}
