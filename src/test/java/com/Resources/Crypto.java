package com.Resources;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.IIOException;
import javax.security.sasl.AuthenticationException;

/**
 * SDP Cryptographic interface.
 *
 * Algorithms must be able to encrypt and decrypt a byte array
 *
 */
public interface Crypto
{
   byte[] encrypt(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException,
         IIOException, SignatureException, Throwable;

   byte[] decrypt(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException,
         AuthenticationException, IIOException, Exception;

   /**
    * Concatenate the byte arrays together.
    *
    * @param byteArrays
    *           - byte arrays to concatenate and encode
    * @return - concatenated byte array
 * @throws Exception 
    * @throws InvalidDataException
    * @throws GeneralSecurityException
    */
   default byte[] concatenateByteArrays(byte[]... byteArrays) throws Exception
   {
      int size = 0;
      for (byte[] byteArray : byteArrays)
      {
         size += byteArray.length;
      }

      if (size < 0)
      {
         throw new Exception();
      }

      ByteBuffer builder = ByteBuffer.allocate(size);
      for (byte[] byteArray : byteArrays)
      {
         builder.put(byteArray);
      }

      return builder.array();
   }
}