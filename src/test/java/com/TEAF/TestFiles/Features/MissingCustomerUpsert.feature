@MF_MissingCustomerUpsert
Feature: Missing Customer Upsert

Description: In this Feature We will be covering Missing Customer Upsert

Scenario:  MF Store Elastic Search Index is to Fetch the datas that are need to be upserted in the Salesfore 
	Given EndPoint 'http://mule-worker-internal-sys-es-dev-v1.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
#	And Update data in the Request 'ElasticSearchIndex' for key 'lastRunDate' with data '$$LastRunDateTime' 
#	And Update data in the Request 'ElasticSearchIndex' for key 'lastProcessedKey' with data '$$LastProcessedKey' 
#	And Update data in the Request 'ElasticSearchIndex' for key 'timest' with data '$$TimeStamp' 
#	And Update data in the Request 'ElasticSearchIndex' for key 'esDeltaQuerySize' with data '2' 
	And Request body 'MissingContactElasticSearchIndex'
	When Method 'Post'
	And Print response
	And Statuscode '200'
	And Retrieve data '$.[1]ES_MODIFIEDDATETIME' from response and store in variable 'ModifiedTime'
	And Print Data 'ES_MODIFIEDDATETIME[1]' 
	And Retrieve data '$.[1]CUST_CD' from response and store in variable 'CustomerID'
	And Print Data 'CUST_CD[1]' 
	And Statuscode '200' 
	And Key '$.[1]CUST_CD' count present in the response - '2'
	And Get the response for Elastic Search Index and store the keys for missing contacts
	

Scenario:  MF Store WarehouseList is to be a Get method gives all the warehouse datas that are available
	Given EndPoint 'http://mule-worker-internal-sys-salesforce-dev-v1.us-w2.cloudhub.io:8091/salesforce/storelist' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	When Method 'Get' 
	And Print response 
	And Statuscode '200' 
	And Get Servicing DC Number from warehouse list
	

Scenario:  MF Store SFCUpsert is to insert the data in the Salesforce with the help of "Elastic Search Index" and "WareHouse List"
	Given EndPoint 'http://mule-worker-internal-sys-salesforce-dev-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update the Missing customer data to SFDC Upsert
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Match JSONPath "$..number_records_processed" contains "2" 