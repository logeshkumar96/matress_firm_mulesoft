@MF_ProductFailureEmail
Feature: ProductUpsert Email Failure Notification 

Description: If there is any Failure it will be send a mail to the mentioned JOB_FAILURE_EMAIL_DISTRO mailID

@ProductFailureEmail
Scenario: MF Product Failure EmailConfiguration and Email will be sent to JOB_FAILURE_EMAIL_DISTRO mailID 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME'
	
	
	#Scenario: MF Product Failure Email Notification
	Given EndPoint 'http://mule-worker-internal-sys-email-dev-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductFailureEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductFailureEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '25' seconds
	And I verify the Failure email is received and read the body content 
	
@InvalidEmail
Scenario: If you pass a Invalid EmailID we will get a error message 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'ProductUpsert/ProductFailureEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'ProductUpsert' present in field '[0].APPLICATION_NAME'
	
	#Scenario: MF Failure Store Email Notification
	Given EndPoint 'http://mule-worker-internal-sys-email-dev-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'ProductUpsert/ProductInvalidEmail' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'ProductUpsert/ProductInvalidEmail' 
	When Method 'Post' 
	And Print response 
	And Statuscode '500'
	And Verify response matches expected text 'Error while sending email: Invalid Addresses' 
#	And Match the response contains the data 'Error while sending email: Invalid Addresses'
	#invalid Email address
	
	
