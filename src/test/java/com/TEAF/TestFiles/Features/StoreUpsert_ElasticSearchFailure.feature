@MF_Store_Failure
Feature: Elastic Search Index Failure Flow : -

Description: If we have no datas a from Elastic Search Index then we wont we able to proceed with the sdfcupsert
	
@ElasticSearchIndexFailure 
Scenario: Store Upsert email Configuration that allows to setup Application Name ,Flow Name ,jobRunEmailDistroFlag ,Email Subject , Email message body content
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/EmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	
	
	#Scenario: MF Store Upsert Flow email distribution list when the job runs successfully
	Given EndPoint 'http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/EmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'StoreUpsert/EmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message'
	And I wait for '25' seconds 
	And I verify the email is received and read the body content
	
	#Scenario: MF Store Upsert Reset Timestamp is to resert the Last Rundate and Last Processed key
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/timestamp' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/StoreTimestamp' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record successfully updated in Service_Run Table.' present in field 'message'
	
	#Scenario: MF Store Insert Log Details is to insert the logs into the database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/insert-log-details' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/InsertLogDetails' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Record inserted in LOG_DETAILS table.' present in field 'message'
	
	#Scenario: MF Store Fetch Service Run is to fetch the LastProcessedkey , Timestamp, LastRunDate ,lastExecDateTime ,lastExecTimeStamp from database
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/fetch-service-run' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/FetchService' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Retrieve data '$.serviceRunRecord[0].lastRunDateTime' from response and store in variable 'LastRunDateTime'
	And Print Data 'serviceRunRecord[0].lastRunDateTime'
	And Retrieve data '$.serviceRunRecord[0].timestamp' from response and store in variable 'TimeStamp'
	And Print Data 'serviceRunRecord[0].timestamp'
	And Retrieve data '$.serviceRunRecord[0].lastProcessedKey' from response and store in variable 'LastProcessedKey'
	And Print Data 'serviceRunRecord[0].lastProcessedKey'
	
	

Scenario: MF Store Elastic Search Index is to Fetch the datas that are need to be upserted in the Salesfore 
	Given EndPoint 'http://mule-worker-internal-sys-es-uat-v1.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/StoreFailureElasticSearch'
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	Then Verify the response body is null 
#	And Verify that value of the field {string} is null


Scenario: MF Failure Store EmailConfiguration 
	Given EndPoint 'http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'StoreUpsert/StoreFailureEmailConfig' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
And Retrieve data '$.[0]FROM_ADDRESS' from response and store in variable 'FROM_ADDRESS'
	And Verify Value 'CustomerPortal' present in field '[0].INTEGRATION_NAME'
	And Verify Value 'StoreUpsert' present in field '[0].APPLICATION_NAME'
	
	
	#Scenario: MF Failure Store Email Notification
	Given EndPoint 'http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Update data in the Request 'StoreUpsert/StoreFailureEmailNotification' for key 'FROM_ADDRESS' with data '$$FROM_ADDRESS' 
	And Request body 'StoreUpsert/StoreFailureEmailNotification' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'Email sent successfully to logeshkumar.r@royalcyber.com' present in field 'message' 
	And I wait for '25' seconds 
	And I verify the Failure email is received and read the body content 
	
	
	
		