@TealiumDevValDB
Feature: Tealium End to End flow  in  Dev environment

Description: This Feature file covers validations of data collection from Customer CrossRef index, Cloudingo SF Backup index and Customer Index and creating a CSV file with required data.

@TealiumScenario1
Scenario: Verify data collection from Customer CrossRef index and store in an array
	Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'customer_cross_ref' exists
	Then Get Result as String by passing Query '{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword":""}},{"range":{"ES_MODIFIEDDATETIME":{"gte":"2020/03/28 22:20:41","lte":"2020/03/28 22:20:41"}}}],"must_not":[ ],"should":[ ]}},"from":0,"size":1000,"sort":[ ],"aggs":{ }}'
	And Print ES Query Response
	When Retrieve Record '$.hits.hits[*]._source.customerCode' from Query Result and store in variable 'TLMUltimateMasterCodes'
	

@TealiumScenario1
Scenario: Verify data collection from Customer CrossRef index and store in an array
	Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'customer_cross_ref' exists
	And Update data in the Request 'TLMCustCrossRef' for key '$.query.bool.must[0].terms["ultimateMasterCustomerCode.keyword"]' with Array data '$$TLMUltimateMasterCodes'
	Then Get Result as String by passing Query '@TLMCustCrossRef'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].key' from Query Result and store in variable 'TLMMasterCodes'
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.customerCode' from Query Result and store in variable 'TLMCustomerCodes'
	And add master codes and customer codes to pass for further queries
	

@TealiumScenario1
Scenario: Verify data collection from Customer CrossRef index and store in an array
	Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cloudingo_sf_backup' exists
	And Update data in the Request 'TLMCloudingoSFBackup' for key '$.query.bool.must[0].terms["customerCode.keyword"]' with Array data '$$Allmasterandcustcodes'
	Then Get Result as String by passing Query '@TLMCloudingoSFBackup'
#	Then Get Result as String by passing Query '{"size":0,"query":{"bool":{"must":[{"terms":{"customerCode.keyword":["SLP-3705885","AX-C005236213","AX-C012236689","AX-C005235881","AX-C012236476","SLP-SYMOD345Z1"]}},{"match":{"MuleRequestType":"POST"}}]}},"aggs":{"group_by_cust_cd":{"terms":{"field":"customerCode.keyword","size":9000},"aggs":{"view":{"top_hits":{"_source":{"include":["customerCode","masterCustomerCode","contactId","masterContactId","type"]},"size":9000}}}}},"sort":[{"ES_MODIFIEDDATETIME":{"order":"desc"}}]}'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].key' from Query Result and store in variable 'TLMResponseCustomerCodes'
	And create arraylist of master codes, customer codes, contactId, masterContactId and type object only DB
	
@TealiumScenario1
Scenario: Verify data collection from Customer CrossRef index and store in an array
	Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cust' exists
	And Update data in the Request 'TLMCustIndex' for key '$.query.bool.must[0].terms["CUST_CD.keyword"]' with Array data '$$Allmasterandcustcodes'
	Then Get Result as String by passing Query '@TLMCustIndex'
	And Print ES Query Response
	And create arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object only DB
	And create CSV file 'C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\Resources\\TealiumCSVfile.csv' using all the datas that are collected