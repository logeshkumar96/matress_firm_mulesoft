@8x8QA
Feature: 8x8 flow in QA environment

Description: This feature file is based on System API


Scenario Outline: Verify the Details of the Store By passing the Store Caller Id to Elastic Search Index using SysemAPI
Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/phoneNumber/store' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body '<RequestBodyFile>' 
	When Method 'Post' 
	And Print response 
	And Statuscode '201'
	And Verify Value '<ID1>' present in field 'op[0].Id'
	And Verify Value '<Name1>' present in field 'op[0].Name'
	And Verify Value '<ID2>' present in field 'op[1].Id'
	And Verify Value '<Name2>' present in field 'op[1].Name'
	And Verify Value 'false' present in field as String 'isSalesOrder'
#	And Verify Value 'null' present in field 'op[0].ClosureDate''
And Verify that value of the field 'op[0].ClosureDate' is null

Examples:
|RequestBodyFile|ID1|Name1|ID2|Name2|
|Storecall_StoreToCustomerCall/8x8SysApi_OnlyStore |166025|Mansfield|AX-166025|Mansfield|
|Storecall_StoreToCustomerCall/8x8SysApi_OnlyStore1|166031|Clinton  |AX-166031|Clinton  |


Scenario: Verify the Details of the Store By passing the Customer Caller Id to Elastic Search Index using SysemAPI
Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/phoneNumber/store' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body 'Storecall_StoreToCustomerCall/8x8SysApi_Customer&Store' 
	When Method 'Post' 
	And Print response
	And Statuscode '201' 	
	And Verify the response body is empty

@QA_8X8Sys
Scenario Outline: Verify the Details of the Customer By passing the Customer Caller Id to Elastic Search Index using SysemAPI
#customer having 3closed order and 1 open order
Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/phoneNumber' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body '<ResponseBody>' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value '<SalesPersonName>' present in field '<SalesPersonName_Field>'
#	And Verify Value '<TotalSale>' present in field '<TotalSale_Field>'
	And Verify that value of the field '<NullField>' is null
	And Verify Value '<Taxchg>' present in field '<Taxchg_Field>'
	And Verify Value '<DEL_Doc_NUM>' present in field '<DEL_Doc_NUM_Field>'
	And Verify Value '<STORECD>' present in field '<STORECD_Field>'
	And Verify that value of the field '<Delieverydate_Field>' is null
#	And Verify Value '<Delieverydate>' present in field '<Delieverydate_Field>'
	And Verify Value '<CUST_CD>' present in field '<CUST_CD_Field>'
	And Verify JSONPath "<CUST_CD_Field>" contains the Prefix "AX-"
#	And Match JSONPath "<CUST_CD_Field>" contains ID "AX-"
	And Verify Value '<SO_WR_DT>' present in field '<SO_WR_DT_Field>'
	And Verify Value '<SalesStatus>' present in field '<SalesStatus_Field>'
	And Verify Value '<CustName>' present in field '<CustName_Field>'
	And Verify Value '<SalesPersonName2>' present in field '<SalesPersonName2_Field>'
	And Verify Value '<isSalesOrder>' present in field '<isSalesOrder_Field>'

	And Verify Value '<SalesPersonName2>' present in field '<SalesPersonName_Field2>'
	And Verify Value '<TotalSale2>' present in field '<TotalSale_Field2>'
	And Verify that value of the field '<NullField1>' is null
	And Verify Value '<Taxchg2>' present in field '<Taxchg_Field2>'
	And Verify Value '<DEL_Doc_NUM2>' present in field '<DEL_Doc_NUM_Field2>'
	And Verify Value '<STORECD2>' present in field '<STORECD_Field2>'
	And Verify Value '<Delieverydate2>' present in field '<Delieverydate_Field2>'
	And Verify Value '<CUST_CD2>' present in field '<CUST_CD_Field2>'
#	And Match JSONPath "<CUST_CD_Field>" contains ID "AX-" 
	And Verify JSONPath "<CUST_CD_Field>" contains the Prefix "AX-"
	And Verify Value '<SO_WR_DT2>' present in field '<SO_WR_DT_Field2>'
	And Verify Value '<SalesStatus2>' present in field '<SalesStatus_Field2>'
	And Verify Value '<CustName2>' present in field '<CustName_Field2>'
	And Verify Value '<SalesPersonName2.2>' present in field '<SalesPersonName2_Field2>'
	
	And Verify Value '<SalesPersonName3>' present in field '<SalesPersonName_Field3>'
	And Verify that value of the field '<NullField2>' is null
	And Verify Value '<Taxchg3>' present in field '<Taxchg_Field3>'
	And Verify Value '<DEL_Doc_NUM3>' present in field '<DEL_Doc_NUM_Field3>'
	And Verify Value '<STORECD3>' present in field '<STORECD_Field3>'
	And Verify Value '<Delieverydate3>' present in field '<Delieverydate_Field3>'
	And Verify Value '<CUST_CD3>' present in field '<CUST_CD_Field3>'
#	And Match JSONPath "<CUST_CD_Field>" contains ID "AX-"
	And Verify JSONPath "<CUST_CD_Field>" contains the Prefix "AX-"
	And Verify Value '<SO_WR_DT3>' present in field '<SO_WR_DT_Field3>'
	And Verify Value '<SalesStatus3>' present in field '<SalesStatus_Field3>'
	And Verify Value '<CustName3>' present in field '<CustName_Field3>'
	And Verify Value '<SalesPersonName2.3>' present in field '<SalesPersonName2_Field3>'
	
	And Verify Value '<SalesPersonName4>' present in field '<SalesPersonName_Field4>'
	And Verify Value '<TotalSale4>' present in field '<TotalSale_Field4>'
	And Verify that value of the field '<NullField3>' is null
	And Verify Value '<Taxchg4>' present in field '<Taxchg_Field4>'
	And Verify Value '<DEL_Doc_NUM4>' present in field '<DEL_Doc_NUM_Field4>'
	And Verify Value '<STORECD4>' present in field '<STORECD_Field4>'
	And Verify Value '<Delieverydate4>' present in field '<Delieverydate_Field4>'
	And Verify Value '<CUST_CD4>' present in field '<CUST_CD_Field4>'
	And Verify JSONPath "<CUST_CD_Field>" contains the Prefix "AX-"
#	And Match JSONPath "<CUST_CD_Field>" contains ID "AX-"
	And Verify Value '<SO_WR_DT4>' present in field '<SO_WR_DT_Field4>'
	And Verify Value '<SalesStatus4>' present in field '<SalesStatus_Field4>'
	And Verify Value '<CustName4>' present in field '<CustName_Field4>'
	And Verify Value '<SalesPersonName2.4>' present in field '<SalesPersonName2_Field4>'

And Verify Value '<SalesPersonName5>' present in field '<SalesPersonName_Field5>'
	And Verify Value '<TotalSale5>' present in field '<TotalSale_Field5>'
	And Verify that value of the field '<NullField4>' is null
	And Verify Value '<Taxchg5>' present in field '<Taxchg_Field5>'
	And Verify Value '<DEL_Doc_NUM5>' present in field '<DEL_Doc_NUM_Field5>'
	And Verify Value '<STORECD5>' present in field '<STORECD_Field5>'
	And Verify Value '<Delieverydate5>' present in field '<Delieverydate_Field5>'
	And Verify Value '<CUST_CD5>' present in field '<CUST_CD_Field5>'
	And Verify JSONPath "<CUST_CD_Field>" contains the Prefix "AX-"
#	And Match JSONPath "<CUST_CD_Field>" contains ID "AX-"
	And Verify Value '<SO_WR_DT5>' present in field '<SO_WR_DT_Field5>'
	And Verify Value '<SalesStatus5>' present in field '<SalesStatus_Field5>'
	And Verify Value '<CustName5>' present in field '<CustName_Field5>'
	And Verify Value '<SalesPersonName2.5>' present in field '<SalesPersonName2_Field5>'
   And Verify Value '<isSalesOrder>' present in field '<isSalesOrder_Field>'
   

Examples:	
|ResponseBody                                          |SalesPersonName         |TotalSale|Taxchg |DEL_Doc_NUM       |STORECD|Delieverydate|CUST_CD      |SO_WR_DT           |SalesStatus|CustName           |SalesPersonName2|SalesPersonName_Field   |TotalSale_Field|Taxchg_Field   |DEL_Doc_NUM_Field|STORECD_Field    |Delieverydate_Field|CUST_CD_Field |SO_WR_DT_Field |SalesStatus_Field |CustName_Field  |SalesPersonName2_Field   |SalesPersonName2|TotalSale2|Taxchg2|DEL_Doc_NUM2     |STORECD2|Delieverydate2|CUST_CD2     |SO_WR_DT2          |SalesStatus2|CustName2         |SalesPersonName2.2|SalesPersonName_Field2   |TotalSale_Field2 |Taxchg_Field2  |DEL_Doc_NUM_Field2|STORECD_Field2    |Delieverydate_Field2|CUST_CD_Field2|SO_WR_DT_Field2|SalesStatus_Field2|CustName_Field2 |SalesPersonName2_Field2  |SalesPersonName3|TotalSale3|Taxchg3|DEL_Doc_NUM3  |STORECD3|Delieverydate3|CUST_CD3      |SO_WR_DT3           |SalesStatus3|CustName3     |SalesPersonName2.3|SalesPersonName_Field3    |TotalSale_Field3  |Taxchg_Field3   |DEL_Doc_NUM_Field3 |STORECD_Field3     |Delieverydate_Field3|CUST_CD_Field3 |SO_WR_DT_Field3 |SalesStatus_Field3|CustName_Field3  |SalesPersonName2_Field3   |SalesPersonName4|TotalSale4|Taxchg4  |DEL_Doc_NUM4 |STORECD4|Delieverydate4|CUST_CD4     |SO_WR_DT4          |SalesStatus4|CustName4     |SalesPersonName2.4|SalesPersonName_Field4   |TotalSale_Field4 |Taxchg_Field4  |DEL_Doc_NUM_Field4|STORECD_Field4    |Delieverydate_Field4|CUST_CD_Field4|SO_WR_DT_Field4|SalesStatus_Field4|CustName_Field4 |SalesPersonName2_Field4 |SalesPersonName5|TotalSale5 |Taxchg5|DEL_Doc_NUM5 |STORECD5|Delieverydate5|CUST_CD5     |SO_WR_DT5          |SalesStatus5|CustName5     |SalesPersonName2.5|SalesPersonName_Field5   |TotalSale_Field5 |Taxchg_Field5  |DEL_Doc_NUM_Field5|STORECD_Field5    |Delieverydate_Field5|CUST_CD_Field5|SO_WR_DT_Field5|SalesStatus_Field5|CustName_Field5 |SalesPersonName2_Field5  |isSalesOrder|isSalesOrder_Field|NullField |NullField1|NullField2      |NullField3      |NullField4|
|Storecall_StoreToCustomerCall/8x8SysApi_Data1         |                        |-3199.0  |0.0    |SLP-98259610-1228 |160005 |null         |SLP-5243325   |2012/12/19        |3          |CATHERINE FERRANTE |                |                        |op[0].TOTAL_SALE|op[0].TAX_CHG |op[0].DEL_DOC_NUM|op[0].SO_STORE_CD|op[0].DELIVERYDATE |op[0].CUST_CD |op[0].SO_WR_DT |op[0].SALESSTATUS |op[0].CUST_NAME |op[0].SALES_PERSON_NAME2 |				|3199.0     |0.0    |SLP-98077839-1112|160005  |null          |SLP-5243325  |2012/11/10         |3           |CATHERINE FERRANTE|                  |op[1].SALES_PERSON_NAME1 |op[1].TOTAL_SALE |op[1].TAX_CHG  |op[1].DEL_DOC_NUM |op[1].SO_STORE_CD |op[1].DELIVERYDATE  |op[1].CUST_CD |op[1].SO_WR_DT |op[1].SALESSTATUS |op[1].CUST_NAME |op[1].SALES_PERSON_NAME2 |                |          |       |              |        |              |              |                    |            |              |                  |                          |                  |                |                   |                   |                    |               |                |	                 |                 |                          |                |          |         |             |        |              |             |                   |            |              |                  |                         |                 |               |                  |                  |                    |              |               |                  |                |                        |                |           |       |             |        |              |             |                   |            |              |                  |                         |                 |               |                  |                  |                    |              |               |                  |                |                         |true        |isSalesOrder      |          |          |                |                |          |
|Storecall_StoreToCustomerCall/8x8SysApi_Data2         |                        |1699.98  |0.0    |SLP-102466345-0324|151004 |null         |SLP-8638816   |2015/03/14        |3          |ALISON KISTLER     |                |op[0].SALES_PERSON_NAME1|op[0].TOTAL_SALE|op[0].TAX_CHG |op[0].DEL_DOC_NUM|op[0].SO_STORE_CD|op[0].DELIVERYDATE |op[0].CUST_CD |op[0].SO_WR_DT |op[0].SALESSTATUS |op[0].CUST_NAME |op[0].SALES_PERSON_NAME2 |				|           |       |                 |        |              |             |                   |            |                  |                  |                         |                 |               |                  |                  |                    |              |               |                  |                |                         |                |          |       |              |        |              |              |                    |            |              |                  |                          |                  |                |                   |                   |                    |               |                |	                 |                 |                          |                |          |         |             |        |              |             |                   |            |              |                  |                         |                 |               |                  |                  |                    |              |               |                  |                |                        |                |           |       |             |        |              |             |                   |            |              |                  |                         |                 |               |                  |                  |                    |              |               |                  |                |                         |true        |isSalesOrder      |          |          |                |                |          |

#|Storecall_StoreToCustomerCall/8x8SysAPI_Data2         |Kaitlyn Stone           |857.28   |77.16  |AX-S028189419 |040002 |2001/01/01   |AX-C013692530|2020/03/25 15:53:39|1          |Kat Uat       |                |op[0].SALES_PERSON_NAME1|op[0].TOTAL_SALE|op[0].TAX_CHG |op[0].DEL_DOC_NUM|op[0].SO_STORE_CD|op[0].DELIVERYDATE |op[0].CUST_CD |op[0].SO_WR_DT |op[0].SALESSTATUS |op[0].CUST_NAME |op[0].SALES_PERSON_NAME2 |Kaitlyn Stone   |2678.99   |221.02 |AX-S028189418|040002  |2001/01/01    |AX-C013692530|2020/03/25 15:50:12|1           |Kat Uat       |                  |op[1].SALES_PERSON_NAME1 |op[1].TOTAL_SALE |op[1].TAX_CHG  |op[1].DEL_DOC_NUM |op[1].SO_STORE_CD |op[1].DELIVERYDATE  |op[1].CUST_CD |op[1].SO_WR_DT |op[1].SALESSTATUS |op[1].CUST_NAME |op[1].SALES_PERSON_NAME2 |Kaitlyn Stone   |2778.99   |221.02 |AX-S028189417 |040002  |2001/01/01    |AX-C013692530 |2020/03/25 15:44:56 |1           |Kat Uat       |                  |op[2].SALES_PERSON_NAME1  |op[2].TOTAL_SALE  |op[2].TAX_CHG   |op[2].DEL_DOC_NUM  |op[2].SO_STORE_CD  |op[2].DELIVERYDATE  |op[2].CUST_CD  |op[2].SO_WR_DT  |op[2].SALESSTATUS  |op[2].CUST_NAME  |op[2].SALES_PERSON_NAME2  |Kaitlyn Stone   |1507.42   |135.67   |AX-S028189416|040002  |2001/01/01    |AX-C013692530|2020/03/25 15:39:13|1           |Kat Uat       |                  |op[3].SALES_PERSON_NAME1 |op[3].TOTAL_SALE |op[3].TAX_CHG  |op[3].DEL_DOC_NUM |op[3].SO_STORE_CD |op[3].DELIVERYDATE  |op[3].CUST_CD |op[3].SO_WR_DT |op[3].SALESSTATUS |op[3].CUST_NAME |op[3].SALES_PERSON_NAME2|Kaitlyn Stone   |2678.99    |221.02 |AX-S028189415|040002  |2001/01/01    |AX-C013692530|2020/03/25 15:31:34|1           |Kat Uat       |                  |op[4].SALES_PERSON_NAME1 |op[4].TOTAL_SALE |op[4].TAX_CHG  |op[4].DEL_DOC_NUM |op[4].SO_STORE_CD |op[4].DELIVERYDATE  |op[4].CUST_CD |op[4].SO_WR_DT |op[4].SALESSTATUS |op[4].CUST_NAME |op[4].SALES_PERSON_NAME2 |true        |isSalesOrder      |	       |          |                |                |          |
 


Scenario Outline: Verify the Details of the Store By passing the Store Caller Id to Elastic Search Index using SysemAPI	
Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/phoneNumber' 
	And Content type 'application/json' 
	And Header key 'Content-Type' value 'application/json' 
	And Request body '<RequestBody>' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value '<Id>' present in field '<Id_Field>'
	And Verify Value '<Name>' present in field as String '<Name_Field>'
	And Verify Value '<OpenDate>' present in field as String '<OpenDate_Field>'
	And Verify that value of the field 'op[3].ClosureDate' is null
	And Verify Value '<City>' present in field '<City_Field>'
	And Verify Value '<Zip>' present in field '<Zip_Field>'
	And Verify Value '<Email>' present in field '<Email_Field>'
	And Verify Value '<Phone>' present in field '<Phone_Field>'
	And Verify Value '<ServicingDCNo>' present in field '<ServicingDCNo_Field>'
	And Verify Value '<AreaManager>' present in field '<AreaManager_Field>'
	And Verify Value '<District>' present in field '<District_Field>'
	And Verify Value '<Type>' present in field '<Type_Field>'
	
And Verify Value '<Id_2>' present in field '<Id_Field_2>'
	And Verify Value '<Name_2>' present in field as String '<Name_Field>'
	And Verify Value '<OpenDate_2>' present in field as String '<OpenDate_Field_2>'
	And Verify that value of the field 'op[3].ClosureDate' is null
	And Verify Value '<City_2>' present in field '<City_Field_2>'
	And Verify Value '<Zip_2>' present in field '<Zip_Field_2>'
	And Verify Value '<Email_2>' present in field '<Email_Field_2>'
	And Verify Value '<Phone_2>' present in field '<Phone_Field_2>'
	And Verify Value '<ServicingDCNo_2>' present in field '<ServicingDCNo_Field_2>'
	And Verify Value '<AreaManager_2>' present in field '<AreaManager_Field_2>'
	And Verify Value '<District_2>' present in field '<District_Field_2>'
	And Verify Value '<Type_2>' present in field '<Type_Field_2>'
	And Verify Value '<isSalesOrder>' present in field '<isSalesOrder_Field>'

	Examples:
	|RequestBody        |StatusCode|Id       |Name    |OpenDate           |City       |Zip     |Email                  |Phone       |ServicingDCNo|AreaManager        |District        |Type  |Id_Field|Name_Field|OpenDate_Field|City_Field|Zip_Field|Email_Field|Phone_Field|ServicingDCNo_Field|AreaManager_Field|District_Field|Type_Field|Id_2       |Name_2    |OpenDate_2           |City_2       |Zip_2     |Email_2                  |Phone_2       |ServicingDCNo_2|AreaManager_2          |District_2        |Type_2  |Id_Field_2|Name_Field_2|OpenDate_Field_2|City_Field_2|Zip_Field_2|Email_Field_2|Phone_Field_2|ServicingDCNo_Field_2|AreaManager_Field_2|District_Field_2|Type_Field_2|isSalesOrder|isSalesOrder_Field|
	|Storecall_StoreToCustomerCall/8x8SysApi_OnlyStore|200       |009027   |Speedway|2013/02/14 00:00:00|Kansas City|66109   |009027@mattressfirm.com|913-299-8448|009800       |Herring, Richard R.|Kansas City-West|Retail|op[0].Id|op[0].Name|op[0].OpenDate|op[0].City|op[0].Zip|op[0].Email|op[0].Phone|op[0].ServicingDCNo|op[0].AreaManager|op[0].District|op[0].Type|AX-009027  |Speedway  |2013/02/14 00:00:00  |Kansas City  |66109     |009027@mattressfirm.com  |913-299-8448  |009800         |Lunceford, Scott       |Kansas City-West  |Retail  |op[1].Id  |op[1].Name  |op[1].OpenDate  |op[1].City  |op[1].Zip  |op[1].Email  |op[1].Phone  |op[1].ServicingDCNo  |op[1].AreaManager  |op[1].District  |op[1].Type  |false       |isSalesOrder      |
	|Storecall_StoreToCustomerCall/8x8_onlyStoreData2 |200       |001023   |Fry Road|1998/03/14 00:00:00|Houston    |77094   |001023@mattressfirm.com|281-599-7908|001000       |Malik, Zaka M.     |Houston-West    |Retail|op[0].Id|op[0].Name|op[0].OpenDate|op[0].City|op[0].Zip|op[0].Email|op[0].Phone|op[0].ServicingDCNo|op[0].AreaManager|op[0].District|op[0].Type|AX-001023  |Fry Road  |1998/03/14 00:00:00  |Houston      |77094     |001023@mattressfirm.com  |281-599-7908  |001000         |Bayack, Christopher C. |Houston-West      |Retail  |op[1].Id  |op[1].Name  |op[1].OpenDate  |op[1].City  |op[1].Zip  |op[1].Email  |op[1].Phone  |op[1].ServicingDCNo  |op[1].AreaManager  |op[1].District  |op[1].Type  |false       |isSalesOrder      |
	|Storecall_StoreToCustomerCall/8x8_onlyStoreData3 |200       |001025   |Pasadena|1999/05/15 00:00:00|Pasadena   |77505   |001025@mattressfirm.com|281-998-9085|001000       |Savoy, Thomas A.   |Houston-East    |Retail|op[0].Id|op[0].Name|op[0].OpenDate|op[0].City|op[0].Zip|op[0].Email|op[0].Phone|op[0].ServicingDCNo|op[0].AreaManager|op[0].District|op[0].Type|AX-001025  |Pasadena  |1999/05/15 00:00:00  |Pasadena     |77505     |001025@mattressfirm.com  |281-998-9085  |001000         |Savoy, Thomas A.       |Houston-East      |Retail  |op[1].Id  |op[1].Name  |op[1].OpenDate  |op[1].City  |op[1].Zip  |op[1].Email  |op[1].Phone  |op[1].ServicingDCNo  |op[1].AreaManager  |op[1].District  |op[1].Type  |false       |isSalesOrder      |
	
	
	