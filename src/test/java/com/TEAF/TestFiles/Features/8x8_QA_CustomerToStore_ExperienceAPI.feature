@8x8QA 
Feature: Customer_To_Store Call using Experience API in QA Environment 

	Description: This feature File covers Experience API for End to End Scenario's between Customer to Store Call which covers all possible combinations & response Validations of Open and closed orders of a Customer

@ExperienceAPI_Cust
Scenario Outline: Validation for Customer having 1 open Order and 3 closed Order using Experience API 
	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	When Query params with Key 'caller' and value '<callerValue>' 
	When Query params with Key 'receiver' and value '<receiverValue>' 
	When Query params with Key 'testing' and value 'true' 
	And Header key 'client_id' value '<client_id>' 
	And Header key 'client_secret' value '<client_secret>'  
	When Method 'Get' 
	And Print response 
	And Retrieve data '$.CUST.OPEN_ORDERS_COUNT' from response and store in variable 'OpenOrderCount'
	And Retrieve data '$.CUST.CLOSED_ORDERS_COUNT' from response and store in variable 'ClosedOrderCount'
	And Statuscode '<StatusCode>' 
	And get the Key value of '<OrderTotal1>' & '<OrderTotal2>' and verify with the total '$.CUST.TOTAL_SALE' 
	And Verify key '$.CLOSED_ORDERS.ORDER_NUMBER' count present in the response orders- '$$ClosedOrderCount' 
	And Verify key '$.OPEN_ORDERS[*].ORDER_NUMBER' count present in the response orders- '$$OpenOrderCount' 
And Key '$.CLOSED_ORDERS.ORDER_NUMBER' List should not contain more than '5' set of records
	And Key '$.OPEN_ORDERS[*].ORDER_NUMBER' List should not contain more than '5' set of records
	And Verify Value '<CustomerName>' present in field 'CUST.CUST_NAME' 
	And Verify Value '<CustomerTotalSale>' present in field 'CUST.TOTAL_SALE' 
	And Verify Value '<CustomerPhoneNumber>' present in field 'CUST.PHONE_NUMBER' 
	And Verify Value '<OpenOrderTotal>' present in field 'CUST.OPEN_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderCount>' present in field as String 'CUST.OPEN_ORDERS_COUNT' 
	And Verify Value '<ClosedOrderCount>' present in field as String 'CUST.CLOSED_ORDERS_COUNT'
	And Verify Value '<ClosedOrderTotal>' present in field 'CUST.CLOSED_ORDERS_TOTAL' 
	And Verify Value '<OpenOrderNumber>' present in field '<OpenOrderNumberField>' 
	And Verify Value '<OpenOrderSalesPersonName1>' present in field '<OpenOrderSalesPersonName1Field>' 
	And Verify Value '<OpenOrderSalesPersonName2>' present in field '<OpenOrderSalesPersonName2Field>' 
	And Verify Value '<OpenOrderStoreCD>' present in field '<OpenOrderStoreCDField>' 
	And Verify Value '<OpenOrderCreatedDate>' present in field '<OpenOrderCreatedDateField>' 
	And Verify Value '<OpenOrderDeliveryDate>' present in field '<OpenOrderDeliveryDateField>' 
	
	
	And Verify Value '<OpenOrderNumber2>' present in field '<OpenOrderNumberField2>' 
	And Verify Value '<OpenOrderSalesPersonName1.2>' present in field '<OpenOrderSalesPersonName1Field2>' 
	And Verify Value '<OpenOrderSalesPersonName2.2>' present in field '<OpenOrderSalesPersonName2Field2>' 
	And Verify Value '<OpenOrderStoreCD2>' present in field '<OpenOrderStoreCDField2>' 
	And Verify Value '<OpenOrderCreatedDate2>' present in field '<OpenOrderCreatedDateField2>' 
	And Verify Value '<OpenOrderDeliveryDate2>' present in field '<OpenOrderDeliveryDateField2>' 
	
	
	And Verify Value '<OpenOrderNumber3>' present in field '<OpenOrderNumberField3>' 
	And Verify Value '<OpenOrderSalesPersonName1.3>' present in field '<OpenOrderSalesPersonName1Field3>' 
	And Verify Value '<OpenOrderSalesPersonName2.3>' present in field '<OpenOrderSalesPersonName2Field3>' 
	And Verify Value '<OpenOrderStoreCD3>' present in field '<OpenOrderStoreCDField3>' 
	And Verify Value '<OpenOrderCreatedDate3>' present in field '<OpenOrderCreatedDateField3>' 
	And Verify Value '<OpenOrderDeliveryDate3>' present in field '<OpenOrderDeliveryDateField3>' 
	
	
	And Verify Value '<OpenOrderNumber4>' present in field '<OpenOrderNumberField4>' 
	And Verify Value '<OpenOrderSalesPersonName1.4>' present in field '<OpenOrderSalesPersonName1Field4>' 
	And Verify Value '<OpenOrderSalesPersonName2.4>' present in field '<OpenOrderSalesPersonName2Field4>' 
	And Verify Value '<OpenOrderStoreCD4>' present in field '<OpenOrderStoreCDField4>' 
	And Verify Value '<OpenOrderCreatedDate4>' present in field '<OpenOrderCreatedDateField4>' 
	And Verify Value '<OpenOrderDeliveryDate4>' present in field '<OpenOrderDeliveryDateField4>' 
	
	And Verify Value '<OpenOrderNumber5>' present in field '<OpenOrderNumberField5>' 
	And Verify Value '<OpenOrderSalesPersonName1.5>' present in field '<OpenOrderSalesPersonName1Field5>' 
	And Verify Value '<OpenOrderSalesPersonName2.5>' present in field '<OpenOrderSalesPersonName2Field5>' 
	And Verify Value '<OpenOrderStoreCD5>' present in field '<OpenOrderStoreCDField5>' 
	And Verify Value '<OpenOrderCreatedDate5>' present in field '<OpenOrderCreatedDateField5>' 
	And Verify Value '<OpenOrderDeliveryDate5>' present in field '<OpenOrderDeliveryDateField5>' 
	
	And Verify Value '<ClosedOrderNumber1>' present in field '<ClosedOrderNumberField1>' 
	And Verify Value '<ClosedOrderNumber2>' present in field '<ClosedOrderNumberField2>' 
	And Verify Value '<ClosedOrderNumber3>' present in field '<ClosedOrderNumberField3>' 

Examples:	
|callerValue|receiverValue|client_id                       |client_secret                   |StatusCode|CustomerName            |CustomerTotalSale|CustomerPhoneNumber|OpenOrderTotal|OpenOrderCount|ClosedOrderCount|ClosedOrderTotal|OpenOrderNumber|OpenOrderNumberField       |OpenOrderSalesPersonName1|OpenOrderSalesPersonName1Field   |OpenOrderSalesPersonName2|OpenOrderSalesPersonName2Field   |OpenOrderStoreCD|OpenOrderStoreCDField     |OpenOrderCreatedDate|OpenOrderCreatedDateField  |OpenOrderDeliveryDate|OpenOrderDeliveryDateField  |OpenOrderNumber2|OpenOrderNumberField2       |OpenOrderSalesPersonName1.2|OpenOrderSalesPersonName1Field2   |OpenOrderSalesPersonName2.2  |OpenOrderSalesPersonName2Field2   |OpenOrderStoreCD2|OpenOrderStoreCDField2     |OpenOrderCreatedDate2|OpenOrderCreatedDateField2  |OpenOrderDeliveryDate2|OpenOrderDeliveryDateField2  |OpenOrderNumber3|OpenOrderNumberField3       |OpenOrderSalesPersonName1.3|OpenOrderSalesPersonName1Field3   |OpenOrderSalesPersonName2.3|OpenOrderSalesPersonName2Field3   |OpenOrderStoreCD3|OpenOrderStoreCDField3     |OpenOrderCreatedDate3|OpenOrderCreatedDateField3  |OpenOrderDeliveryDate3|OpenOrderDeliveryDateField3  |OpenOrderNumber4  |OpenOrderNumberField4       |OpenOrderSalesPersonName1.4|OpenOrderSalesPersonName1Field4   |OpenOrderSalesPersonName2.4|OpenOrderSalesPersonName2Field4   |OpenOrderStoreCD4|OpenOrderStoreCDField4     |OpenOrderCreatedDate4|OpenOrderCreatedDateField4  |OpenOrderDeliveryDate4|OpenOrderDeliveryDateField4  |OpenOrderNumber5  |OpenOrderNumberField5       |OpenOrderSalesPersonName1.5 |OpenOrderSalesPersonName1Field5   |OpenOrderSalesPersonName2.5  |OpenOrderSalesPersonName2Field5   |OpenOrderStoreCD5|OpenOrderStoreCDField5     |OpenOrderCreatedDate5|OpenOrderCreatedDateField5  |OpenOrderDeliveryDate5|OpenOrderDeliveryDateField5  |ClosedOrderNumber1|ClosedOrderNumber2|ClosedOrderNumber3|ClosedOrderNumberField1      |ClosedOrderNumberField2      |ClosedOrderNumberField3   |OrderTotal1               |OrderTotal2               |
|12254585758|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |JOHN DOE                |560.38           |+1 (225) 458-5758  |560.38        |1             |0               |0               |AX-WEB00091404 |OPEN_ORDERS[0].ORDER_NUMBER|                         |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|290007          |OPEN_ORDERS[0].SO_STORE_CD|03/23/2020          |OPEN_ORDERS[0].CREATED_DATE|12/31/2049           |OPEN_ORDERS[0].DELIVERY_DATE|                |                            |                           |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |                |                            |                           |                                  |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                            |                           |                                  |                           |                                  |                 |                           |                     |                            |                      |                             |                  |                            |                            |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |                  |                  |                  |                             |                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|
|13453548880|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |MarchRelease UAT Testing|14538.85         |+1 (345) 354-8880  |11725.43      |4             |1               |2813.42         |AX-S028189376  |OPEN_ORDERS[0].ORDER_NUMBER|                         |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|001070          |OPEN_ORDERS[0].SO_STORE_CD|03/24/2020          |OPEN_ORDERS[0].CREATED_DATE|01/01/2001           |OPEN_ORDERS[0].DELIVERY_DATE|AX-S028189366   |OPEN_ORDERS[1].ORDER_NUMBER |                           |OPEN_ORDERS[1].SALES_PERSON_NAME1 |Patsy Kennedy                |OPEN_ORDERS[1].SALES_PERSON_NAME2 |001070           |OPEN_ORDERS[1].SO_STORE_CD |03/23/2020           |OPEN_ORDERS[1].CREATED_DATE |01/01/2001            |OPEN_ORDERS[1].DELIVERY_DATE |AX-S028189364   |OPEN_ORDERS[2].ORDER_NUMBER |                           |OPEN_ORDERS[2].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[2].SALES_PERSON_NAME2 |001070           |OPEN_ORDERS[2].SO_STORE_CD |03/23/2020           |OPEN_ORDERS[2].CREATED_DATE |01/01/2001            |OPEN_ORDERS[2].DELIVERY_DATE |AX-S028189348     |OPEN_ORDERS[3].ORDER_NUMBER |                           |OPEN_ORDERS[3].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[3].SALES_PERSON_NAME2 |001070           |OPEN_ORDERS[3].SO_STORE_CD |03/23/2020           |OPEN_ORDERS[3].CREATED_DATE |01/01/2001            |OPEN_ORDERS[3].DELIVERY_DATE |                  |                            |                            |                                  |                             |                                  |                 |                           |                     |                            |                      |                             |AX-S028189372     |                  |                  |CLOSED_ORDERS.ORDER_NUMBER[0]|                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|                          
|18882223335|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Kat Uat                 |11377.56         |+1 (888) 222-3335  |11377.56      |5             |0               |0               |AX-S028189419  |OPEN_ORDERS[0].ORDER_NUMBER|Kaitlyn Stone            |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|040002          |OPEN_ORDERS[0].SO_STORE_CD|03/25/2020          |OPEN_ORDERS[0].CREATED_DATE|01/01/2001           |OPEN_ORDERS[0].DELIVERY_DATE|AX-S028189418   |OPEN_ORDERS[1].ORDER_NUMBER |Kaitlyn Stone              |OPEN_ORDERS[1].SALES_PERSON_NAME1 |                             |OPEN_ORDERS[1].SALES_PERSON_NAME2 |040002           |OPEN_ORDERS[1].SO_STORE_CD |03/25/2020           |OPEN_ORDERS[1].CREATED_DATE |01/01/2001            |OPEN_ORDERS[1].DELIVERY_DATE |AX-S028189417   |OPEN_ORDERS[2].ORDER_NUMBER |Kaitlyn Stone              |OPEN_ORDERS[2].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[2].SALES_PERSON_NAME2 |040002           |OPEN_ORDERS[2].SO_STORE_CD |03/25/2020           |OPEN_ORDERS[2].CREATED_DATE |01/01/2001            |OPEN_ORDERS[2].DELIVERY_DATE |AX-S028189416     |OPEN_ORDERS[3].ORDER_NUMBER |Kaitlyn Stone              |OPEN_ORDERS[3].SALES_PERSON_NAME1 |                           |OPEN_ORDERS[3].SALES_PERSON_NAME2 |040002           |OPEN_ORDERS[3].SO_STORE_CD |03/25/2020           |OPEN_ORDERS[3].CREATED_DATE |01/01/2001            |OPEN_ORDERS[3].DELIVERY_DATE |AX-S028189415     |OPEN_ORDERS[4].ORDER_NUMBER |Kaitlyn Stone               |OPEN_ORDERS[4].SALES_PERSON_NAME1 |                             |OPEN_ORDERS[4].SALES_PERSON_NAME2 |040002           |OPEN_ORDERS[4].SO_STORE_CD |03/25/2020           |OPEN_ORDERS[4].CREATED_DATE |01/01/2001            |OPEN_ORDERS[4].DELIVERY_DATE |                  |                  |                  |                             |                             |                          |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|

#|14173316309|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Colleen Rantz |1154.63          |+1 (417) 331-6309  |0             |1             |3               |1154.63         |AX-S007345376  |OPEN_ORDERS[0].ORDER_NUMBER|Brian Shutt              |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|072005          |OPEN_ORDERS[0].SO_STORE_CD|07/06/2017          |OPEN_ORDERS[0].CREATED_DATE|01/01/2001           |OPEN_ORDERS[0].DELIVERY_DATE|AX-S007613424     |AX-S007347431     |AX-S007248305     |CLOSED_ORDERS.ORDER_NUMBER[0]|CLOSED_ORDERS.ORDER_NUMBER[1]|CLOSED_ORDERS.ORDER_NUMBER[2]|$.CUST.CLOSED_ORDERS_TOTAL|$.CUST.OPEN_ORDERS_TOTAL  |
#|17728280865|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Taylor Gordon |1197.77          |+1 (772) 828-0865  |1197.77       |1             |0               |0               |AX-S007001426  |OPEN_ORDERS[0].ORDER_NUMBER|Theodore Seger           |OPEN_ORDERS[0].SALES_PERSON_NAME1|                         |OPEN_ORDERS[0].SALES_PERSON_NAME2|053035          |OPEN_ORDERS[0].SO_STORE_CD|06/02/2017          |OPEN_ORDERS[0].CREATED_DATE|01/01/2001           |OPEN_ORDERS[0].DELIVERY_DATE|                  |                  |                  |                             |                             |                             |$.CUST.OPEN_ORDERS_TOTAL  |$.CUST.CLOSED_ORDERS_TOTAL|
#|13123838806|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Yash Ganorkar |264.56           |+1 (312) 383-8806  |0             |0             |1               |                |               |                           |                         |                                 |                         |                                 |                |                          |                    |                           |                     |                            |AX-S004560739     |                  |                  |CLOSED_ORDERS.ORDER_NUMBER[0]|                             |                             |$.CUST.CLOSED_ORDERS_TOTAL|$.CUST.OPEN_ORDERS_TOTAL  |
#|18477570558|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Raul Caceres  |350              |+1 (847) 757-0558  |0             |0             |1               |350             |               |                           |                         |                                 |                         |                                 |                |                          |                    |                           |                     |                            |AX-S007059317     |                  |                  |CLOSED_ORDERS.ORDER_NUMBER[0]|                             |                             |$.CUST.CLOSED_ORDERS_TOTAL|$.CUST.OPEN_ORDERS_TOTAL  |
#|13479388954|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Miguel Bonilla|772.99           |+1 (347) 938-8954  |0             |0             |2               |772.99          |               |                           |                         |                                 |                         |                                 |                |                          |                    |                           |                     |                            |AX-S007108403     |AX-S007059256     |                  |CLOSED_ORDERS.ORDER_NUMBER[0]|CLOSED_ORDERS.ORDER_NUMBER[1]|                             |$.CUST.CLOSED_ORDERS_TOTAL|$.CUST.OPEN_ORDERS_TOTAL  |
#|18177760355|12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |Araceli Torres|4491.3           |+1 (817) 776-0355  |0             |0             |3               |4491.3          |               |                           |                         |                                 |                         |                                 |                |                          |                    |                           |                     |                            |AX-S025588333     |AX-S025575628     |AX-S001948209     |CLOSED_ORDERS.ORDER_NUMBER[0]|CLOSED_ORDERS.ORDER_NUMBER[1]|CLOSED_ORDERS.ORDER_NUMBER[2]|$.CUST.CLOSED_ORDERS_TOTAL|$.CUST.OPEN_ORDERS_TOTAL  |
	
	
Scenario Outline: Validation for Customer to Store Call with multiple Combination of Caller and Reciever datas using Experience API 
	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	When Query params with Key 'caller' and value '<callerValue>' 
	When Query params with Key 'receiver' and value '<receiverValue>' 
	When Query params with Key 'testing' and value 'true' 
	And Header key 'client_id' value '<client_id>' 
	And Header key 'client_secret' value '<client_secret>' 
	When Method 'Get' 
	And Print response 
	And Statuscode '<StatusCode>' 
	And Verify response matches expected text '<Message>'
#	And Match the response contains the data '<Message>' 
	
	Examples: 
		|callerValue|receiverValue|client_id                       |client_secret                   |StatusCode|Message               |
		|           |12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |"No Data for Receiver"|
		|18177760355|             |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |"No data for Receiver"|
		|111222333  |12064641513  |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |"No Data for Caller"  |
		|14173316309|11223344     |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |"No data for Receiver"|	
		|14173316   |11112222     |2fcfdbf7f04643dc91e3975a1ff6efe7|4E9DB968A88B4b929A40FDEaf6b010b6|200       |"No Data for Caller"  |
		
		
		
Scenario: Validation for Customer to Store Call by without sending the "caller" input parameter using Experience API 

	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	#	When Query params with Key 'caller' and value '' 
	When Query params with Key 'receiver' and value '12064641513' 
	When Query params with Key 'testing' and value 'true' 
	And Header key 'client_id' value '2fcfdbf7f04643dc91e3975a1ff6efe7' 
	And Header key 'client_secret' value '4E9DB968A88B4b929A40FDEaf6b010b6' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
And Verify Value 'Bad request' present in field 'message'
	
Scenario: Validation for Customer to Store Call by without sending the "receiver" input parameter using Experience API 
	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	When Query params with Key 'caller' and value '18177760355' 
	#	When Query params with Key 'receiver' and value '' 
	When Query params with Key 'testing' and value 'true' 
	And Header key 'client_id' value '2fcfdbf7f04643dc91e3975a1ff6efe7' 
	And Header key 'client_secret' value '4E9DB968A88B4b929A40FDEaf6b010b6' 
	When Method 'Get' 
	And Print response 
	And Statuscode '400' 
And Verify Value 'Bad request' present in field 'message'	
	
Scenario: Validation for Customer to Store Call by without sending the client_id and client_secret using Experience API 
	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	When Query params with Key 'caller' and value '14173316309' 
	When Query params with Key 'receiver' and value '12064641513' 
	When Query params with Key 'testing' and value 'true' 
	#	And Header key 'client_id' value '2fcfdbf7f04643dc91e3975a1ff6efe7' 
	#	And Header key 'client_secret' value '4E9DB968A88B4b929A40FDEaf6b010b6' 
	When Method 'Get' 
	And Print response 
	And Statuscode '401' 
	And Match JSONPath "error" contains "Authentication denied." 
	
Scenario: Validation for Customer to Store Call with the Restricted Caller using Experience API 
	Given EndPoint 'https://exp-customer-info-dev.ms.sandbox.mfrm.com/api/test8by8' 
	When Query params with Key 'caller' and value 'Restricted' 
	When Query params with Key 'receiver' and value '12064641513' 
	When Query params with Key 'testing' and value 'true' 
	And Header key 'client_id' value '2fcfdbf7f04643dc91e3975a1ff6efe7' 
	And Header key 'client_secret' value '4E9DB968A88B4b929A40FDEaf6b010b6' 
	When Method 'Get' 
	And Print response 
	And Statuscode '200' 
	And Match the response contains the key 'RECIPIENT' 
	And Verify Value '508044' present in field 'RECIPIENT' 
	
	
	
	
	