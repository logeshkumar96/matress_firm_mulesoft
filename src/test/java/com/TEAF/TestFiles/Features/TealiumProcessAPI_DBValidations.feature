@TealiumProcessAPIDBVal
Feature: Tealium end to End flow with Process and System API's Validation with both DB and API response

Description: This Feature file covers validations of Tealium Process API

@TealiumProcessAPIScenario2Val
Scenario: Validate data collection from Customer CrossRef index in both API and DB response
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/tealium/crossref' 
    And Content type 'application/json' 
    And Request body 'Tealium/CrossRef'
    When Method 'Post' 
    And Print response 
    And Statuscode '200' 
    And Retrieve data '$..customerCode' from response and store in variable 'APIResponse'
    And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
    And Select and Verify the index 'customer_cross_ref' exists
    Then Get Result as String by passing Query '{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword":""}},{"range":{"ES_MODIFIEDDATETIME":{"gte":"2020/03/28 22:20:41","lte":"2020/03/28 22:20:41"}}}],"must_not":[ ],"should":[ ]}},"from":0,"size":1000,"sort":[ ],"aggs":{ }}'
    And Print ES Query Response
    When Retrieve Record '$.hits.hits[*]._source.customerCode' from Query Result and store in variable 'DBQueryResponse'
     When Retrieve Record '$.hits.hits[*]._source.customerCode' from Query Result and store in variable 'DBMasterCodes'
    And Compare API response with DB query Response
    

@TealiumProcessAPIScenario3Val
Scenario: Validate data collection from Customer CrossRef index in both API and DB response
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/relatedContact' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CustomerCode' for key '$.customerCodes' with Array data '$$APIResponse'
    And Request body 'Tealium/CustomerCode'
    When Method 'Post' 
    And Print response 
    And Statuscode '200'
	And Retrieve data '$.[*].[*]' from response and store in variable 'TLMCustomerCodes'
	And Add master codes and customer codes to pass for further queries in DB Validations
	And Retrieve data '$.[*].[*]' from response and store in variable 'APIResponse'
	And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'customer_cross_ref' exists
	And Update data in the Request 'CustomerCode' for key '$.query.bool.must[0].terms["ultimateMasterCustomerCode.keyword"]' with Array data '$$DBQueryResponse'
	Then Get Result as String by passing Query '@CustomerCode'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.customerCode' from Query Result and store in variable 'DBQueryResponse'
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.customerCode' from Query Result and store in variable 'DBCustomerCodes'
	And Add master codes and customer codes to pass for further queries in DB Validations with DB inputs
	 And Compare API response with DB query Response

@TealiumProcessAPIScenario4Val
Scenario: Validate data collection from Cloudingo SF backup index in both API and DB response
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/salesforceContactId' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CloudingoSFBackup' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CloudingoSFBackup'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And Retrieve data '$.[*]..contactId' from response and store in variable 'APIResponse'
	And ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cloudingo_sf_backup' exists
	And Update data in the Request 'CloudingoSFBackup' for key '$.query.bool.must[0].terms["customerCode.keyword"]' with Array data '$$DBAllmasterandcustcodes'
	
	
	Then Get Result as String by passing Query '@CloudingoSFBackup'
	And Print ES Query Response
	When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.contactId' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
    And Retrieve data '$.[*]..masterContactId' from response and store in variable 'APIResponse'
    When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.masterContactId' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
    And Retrieve data '$.[*]..type' from response and store in variable 'APIResponse'
    When Retrieve Record '$.aggregations.group_by_cust_cd.buckets[*].view.hits.hits[*]._source.type' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
    
@TealiumProcessAPIScenario5
Scenario: Validate data collection from Cust index in both API and DB responsee
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer' 
    And Content type 'application/json'
    And Update data in the Request 'Tealium/CustIndex' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CustIndex'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And Retrieve data '$.[*].[*].ZIP_CD' from response and store in variable 'APIResponse'
    Given ES Search 'https://vpc-qa-ticketdetail-mfrm-65ktt22x2o4nram4vlhrb6kqr4.us-west-2.es.amazonaws.com/'
	And Select and Verify the index 'cust' exists
	And Update data in the Request 'TLMCustIndex' for key '$.query.bool.must[0].terms["CUST_CD.keyword"]' with Array data '$$DBAllmasterandcustcodes'
	Then Get Result as String by passing Query '@TLMCustIndex'
	And Print ES Query Response
     When Retrieve Record '$.aggregations.cust_cd.buckets[*].view.hits.hits[*]._source.ZIP_CD' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
    
And Retrieve data '$.[*].[*].EMAIL_ADDR' from response and store in variable 'APIResponse'
    When Retrieve Record '$.aggregations.cust_cd.buckets[*].view.hits.hits[*]._source.EMAIL_ADDR' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
   
   And Retrieve data '$.[*].[*].HOME_PHONE' from response and store in variable 'APIResponse'
    When Retrieve Record '$.aggregations.cust_cd.buckets[*].view.hits.hits[*]._source.HOME_PHONE' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response
   
    And Retrieve data '$.[*].[*].BUS_PHONE' from response and store in variable 'APIResponse'
    When Retrieve Record '$.aggregations.cust_cd.buckets[*].view.hits.hits[*]._source.BUS_PHONE' from Query Result and store in variable 'DBQueryResponse'
    And Compare API response with DB query Response