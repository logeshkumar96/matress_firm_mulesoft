@TealiumProcessAPIValidations
Feature: Tealium flow includes Process and System API's Validations 

	Description: This Feature file covers validations of Tealium Process API
	
Scenario: Validations for  data collection from Customer CrossRef index 
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/tealium/crossref' 
	And Content type 'application/json' 
	And Request body 'Tealium/CrossRef' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'AX-C012236689' present in field '[0].customerCode' 
	And Verify Value '' present in field '[0].masterCustomerCode' 
	And Verify Value '' present in field '[0].ultimateMasterCustomerCode' 
	And Verify Value '2020/03/28 22:20:41' present in field '[0].ES_MODIFIEDDATETIME' 
	
	And Verify Value 'SLP-3705885' present in field '[1].customerCode' 
	And Verify Value '' present in field '[1].masterCustomerCode' 
	And Verify Value '' present in field '[1].ultimateMasterCustomerCode' 
	And Verify Value '2020/03/28 22:20:41' present in field '[1].ES_MODIFIEDDATETIME' 
	
	And Verify Value 'AX-C005236213' present in field '[2].customerCode' 
	And Verify Value '' present in field '[2].masterCustomerCode' 
	And Verify Value '' present in field '[2].ultimateMasterCustomerCode' 
	And Verify Value '2020/03/28 22:20:41' present in field '[2].ES_MODIFIEDDATETIME' 
	
	
	
Scenario: Validations for data collection from Customer CrossRef index 
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/relatedContact' 
	And Content type 'application/json' 
	And Request body 'Tealium/CustomerCodeValidations' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'AX-C005235881' present in field '[0].AX-C005236213[0]' 
	And Verify Value 'AX-C012236476' present in field '[1].AX-C012236689[0]' 
	And Verify Value 'SLP-SYMOD345Z1' present in field '[2].SLP-3705885[0]' 

@InValidvalidation
Scenario: Verify the response Invalid Master customer code and Valid  Master customer code
Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/relatedContact' 
	And Content type 'application/json' 
	And Request body 'Tealium/CustomerCodeInvalidValidations' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value 'AX-C012236476' present in field '[0].AX-C012236689[0]' 
	And Verify Value 'SLP-SYMOD345Z1' present in field '[1].SLP-3705885[0]' 
		
Scenario: Validations for data collection from Cloudingo SF backup index and store in an array 
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/salesforceContactId' 
	And Content type 'application/json' 
	And Request body 'Tealium/CloudingoSFBackupValidations' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	And Verify Value '0030f00002mFAWJAA4' present in field '[0].AX-C005235881.contactId' 
	And Verify Value '0030f00002m6Re7AAE' present in field '[0].AX-C005235881.masterContactId' 
	And Verify Value 'DUPLICATE' present in field '[0].AX-C005235881.type' 
	And Verify Value 'AX-C005235881' present in field '[0].AX-C005235881.customerCode' 
	And Verify Value 'AX-C005236213' present in field '[0].AX-C005235881.masterCustomerCode' 
	
	And Verify Value '0030f00002m6Re7AAE' present in field '[1].AX-C005236213.contactId' 
	And Verify Value '' present in field '[1].AX-C005236213.masterContactId' 
	And Verify Value 'MASTER' present in field '[1].AX-C005236213.type' 
	And Verify Value 'AX-C005236213' present in field '[1].AX-C005236213.customerCode' 
	And Verify Value '' present in field '[1].AX-C005236213.masterCustomerCode' 
	
	And Verify Value '0030f000034wWNbAAM' present in field '[2].AX-C012236476.contactId' 
	And Verify Value '0030f000034wWhjAAE' present in field '[2].AX-C012236476.masterContactId' 
	And Verify Value 'DUPLICATE' present in field '[2].AX-C012236476.type' 
	And Verify Value 'AX-C012236476' present in field '[2].AX-C012236476.customerCode' 
	And Verify Value 'AX-C012236689' present in field '[2].AX-C012236476.masterCustomerCode' 
	
	And Verify Value '0030f000034wWhjAAE' present in field '[3].AX-C012236689.contactId' 
	And Verify Value '' present in field '[3].AX-C012236689.masterContactId' 
	And Verify Value 'MASTER' present in field '[3].AX-C012236689.type' 
	And Verify Value 'AX-C012236689' present in field '[3].AX-C012236689.customerCode' 
	And Verify Value '' present in field '[3].AX-C012236689.masterCustomerCode' 
	
	And Verify Value '003G000001DY5YRIA1' present in field '[4].SLP-3705885.contactId' 
	And Verify Value '' present in field '[4].SLP-3705885.masterContactId' 
	And Verify Value 'MASTER' present in field '[4].SLP-3705885.type' 
	And Verify Value 'SLP-3705885' present in field '[4].SLP-3705885.customerCode' 
	And Verify Value '' present in field '[4].SLP-3705885.masterCustomerCode' 
	
	And Verify Value '0030f000035R8IqAAK' present in field '[5].SLP-SYMOD345Z1.contactId' 
	And Verify Value '003G000001DY5YRIA1' present in field '[5].SLP-SYMOD345Z1.masterContactId' 
	And Verify Value 'DUPLICATE' present in field '[5].SLP-SYMOD345Z1.type' 
	And Verify Value 'SLP-SYMOD345Z1' present in field '[5].SLP-SYMOD345Z1.customerCode' 
	And Verify Value 'SLP-3705885' present in field '[5].SLP-SYMOD345Z1.masterCustomerCode' 
	
	
Scenario: Verify data collection from Cust index and store in an array to create CSV file 
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer' 
	And Content type 'application/json' 
	And Request body 'Tealium/CustIndexValidations' 
	When Method 'Post' 
	And Print response 
	And Statuscode '200' 
	
	And Verify Value '' present in field '[0].AX-C005235881.ZIP_CD' 
	And Verify Value 'dniedermeyer@theprivatebank.com' present in field '[0].AX-C005235881.EMAIL_ADDR' 
	And Verify Value '773-559-6873' present in field '[0].AX-C005235881.HOME_PHONE' 
	And Verify Value '' present in field '[0].AX-C005235881.BUS_PHONE' 
	
	And Verify Value '60525' present in field '[1].AX-C005236213.ZIP_CD' 
	And Verify Value 'dniedermeyer@theprivatebank.com' present in field '[1].AX-C005236213.EMAIL_ADDR' 
	And Verify Value '773-559-6873' present in field '[1].AX-C005236213.HOME_PHONE' 
	And Verify Value '' present in field '[1].AX-C005236213.BUS_PHONE' 
	
	And Verify Value '08804' present in field '[2].SLP-3705885.ZIP_CD' 
	And Verify Value 'dfs0205@gmail.com' present in field '[2].SLP-3705885.EMAIL_ADDR' 
	And Verify Value '(908) 310-5460' present in field '[2].SLP-3705885.HOME_PHONE' 
	And Verify Value '(908) 310-5460' present in field '[2].SLP-3705885.BUS_PHONE' 
	
	And Verify Value '08804' present in field '[3].SLP-SYMOD345Z1.ZIP_CD' 
	And Verify Value 'dfs0205@gmail.com' present in field '[3].SLP-SYMOD345Z1.EMAIL_ADDR' 
	And Verify Value '9083105460' present in field '[3].SLP-SYMOD345Z1.HOME_PHONE' 
	And Verify Value '' present in field '[3].SLP-SYMOD345Z1.BUS_PHONE' 
	
   