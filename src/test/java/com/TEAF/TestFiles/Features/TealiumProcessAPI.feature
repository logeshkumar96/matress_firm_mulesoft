@TealiumProcessAPI
Feature: Tealium End to End flow which includes Process and System API's

Description: This Feature file covers validations of Tealium Process API

@TealiumProcessAPIScenario1
Scenario: Verify data collection from Customer CrossRef index and store in an array
	Given EndPoint 'http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/tealium/crossref' 
    And Content type 'application/json' 
    And Request body 'Tealium/CrossRef'
    When Method 'Post' 
    And Print response 
    And Statuscode '200' 
    And Retrieve data '$..customerCode' from response and store in variable 'TLMMasterCodes'

@TealiumProcessAPIScenario2
Scenario: Verify data collection from Customer CrossRef index for master codes and store in an array
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/relatedContact' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CustomerCode' for key '$.customerCodes' with Array data '$$TLMMasterCodes'
    And Request body 'Tealium/CustomerCode'
    When Method 'Post' 
    And Print response 
    And Statuscode '200'
	And Retrieve data '$.[*].[*]' from response and store in variable 'TLMCustomerCodes'
	And add master codes and customer codes to pass for further queries

@TealiumProcessAPIScenario3
Scenario: Verify data collection from Cloudingo SF backup index and store in an array
	Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer/salesforceContactId' 
    And Content type 'application/json' 
    And Update data in the Request 'Tealium/CloudingoSFBackup' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CloudingoSFBackup'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And Retrieve data '$.[*]..customerCode' from response and store in variable 'TLMResponseCustomerCodes'
    And create API response arraylist of master codes, customer codes, contactId, masterContactId and type object
    
@TealiumProcessAPIScenario4
Scenario: Verify data collection from Cust index and store in an array to create CSV file
    Given EndPoint 'http://mule-worker-internal-proc-customer-info-api-qa-v1.us-w2.cloudhub.io:8091/api/svoc/customer' 
    And Content type 'application/json'
    And Update data in the Request 'Tealium/CustIndex' for key '$.customerCodes' with Array data '$$Allmasterandcustcodes'
    And Request body 'Tealium/CustIndex'
    When Method 'Post' 
	And Print response 
    And Statuscode '200'
    And create API response arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object
    And create CSV file 'C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\Resources\\TealiumCSVfile.csv' using all the datas that are collected
