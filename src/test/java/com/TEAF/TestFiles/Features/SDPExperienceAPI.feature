@SDP
Feature: SDP End to End flow 

Description: This Feature file covers validations of SDP experience API

@SDPScenario1
Scenario Outline: Verify Experience API storing the SDP data in elastic search index 'synchronyapplication'
	Given EndPoint 'https://exp-sdp-dev.ms.sandbox.mfrm.com/api/credit-decision'
	And Set the Timestamp to 'UTC' 'yyyyMMddHHmmss' -10
	When Create multiple set of SDP data's with parameterization '<ApplicationDecision>''<AccountNumber>''<PermanentExpirationDate>''<CVV>''<ApplicationKey>''<FirstName>''<MiddleInitial>''<LastName>''<HomeAddress>''<HomeAddress2/Apt/Suite>''<City>''<State>''<ZipCode>''<Emailaddress>''<PostbackReferenceID>''<PrimaryPhoneNumber>''<SaleAmount/RequestedSaleAmount>''<SiteCode>''<Timestamp>'
	When Query params with Key 'SDP' and value '$$SDPValue'
	And Header key 'client_id' value '8c7bfa646174432485a8428a656ad285'
	And Header key 'client_secret' value '5B53728899f6444AA00fC7d95307B8Ac'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Verify Value 'Request Received' present in field 'message'
	When Jest Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Jest Print Response

And Verify query result contains Key '<AccountNumberField>' & Value '<AccountNumberDB>'
And Verify query result contains Key '<PermanentExpirationDateField>' & Value '<PermanentExpirationDateDB>'
And Verify query result contains Key '<CVVField>' & Value '<CVVDB>'

And Verify query result contains Key '<ApplicationDecisionField>' & Value '<ApplicationDecision>'
And Verify query result contains Key '<FirstNameField>' & Value '<FirstName>'
And Verify query result contains Key '<MiddleInitialField>' & Value '<MiddleInitial>'
And Verify query result contains Key '<LastNameField>' & Value '<LastName>'
And Verify query result contains Key '<HomeAddressField>' & Value '<HomeAddress>'
And Verify query result contains Key '<HomeAddress2/Apt/SuiteField>' & Value '<HomeAddress2/Apt/Suite>'
And Verify query result contains Key '<CityField>' & Value '<City>'
And Verify query result contains Key '<StateField>' & Value '<State>'
And Verify query result contains Key '<ZipCodeField>' & Value '<ZipCode>'
And Verify query result contains Key '<EmailaddressField>' & Value '<Emailaddress>'
And Verify query result contains Key '<PostbackReferenceIDField>' & Value '<PostbackReferenceID>'
And Verify query result contains Key '<PrimaryPhoneNumberField>' & Value '<PrimaryPhoneNumber>'
And Verify query result contains Key '<SaleAmount/RequestedSaleAmountField>' & Value '<SaleAmount/RequestedSaleAmount>'
And Verify query result contains Key '<SiteCodeField>' & Value '<SiteCode>'
And Verify query result contains Key '<TimestampField>' & Value '<TimestampCST>'

Examples:
|QueryBody                                                                  |Table                   |AccountNumberDB|PermanentExpirationDateDB|CVVDB |ApplicationDecision|AccountNumber   |PermanentExpirationDate|CVV|ApplicationKey|FirstName|MiddleInitial|LastName|HomeAddress    |HomeAddress2/Apt/Suite|City     |State|ZipCode|Emailaddress |PostbackReferenceID|PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode  |Timestamp     |ApplicationDecisionField                |FirstNameField                |MiddleInitialField                |LastNameField                |HomeAddressField                |HomeAddress2/Apt/SuiteField      |CityField                |StateField                |ZipCodeField            |EmailaddressField         |PostbackReferenceIDField          |PrimaryPhoneNumberField         |SaleAmount/RequestedSaleAmountField|SiteCodeField                |TimestampField                |TimestampCST       |AccountNumberField                |PermanentExpirationDateField                     |CVVField                | 
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678444"]}}]}}}|synchronyapplication    |null           |null                     |null  |A                  |1234567898765456|2210                   |123|987678444     |Andrew   |A            |Jerry   |234 Main Street|APT 3A                |Wayne    |NJ   |11245  |aj@sys.com   |12345A BCDE        |7894635267        |3234.32                       |BCDE 1234 |20200326184500|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["458756879"]}}]}}}|synchronyapplication	 |null           |null                     |null  |R                  |0985469806546564|2003                   |000|458756879     |Drew     |S            |Furry   |Main Street    |SDTFO98478UI          |Gotim    |AP   |10004  |kysur@sys.com|ERTYU987574        |1276565645        |4496.49                       |WERT98794 |20200324000000|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["000088887"]}}]}}}|synchronyapplication	 |null           |null                     |null  |D                  |0000044449999999|1910                   |987|000088887     |E        |K            |Korry   |454 SecondCross|DPT 4598GK            |Nasur    |KA   |11226  |rajuk@sys.com|1212UIHJ 2D        |5656767877        |1001.01                       |HJUIY9TY78|20200324000000|hits.hits[0]._source.ApplicationDecision|hits.hits[0]._source.FirstName|hits.hits[0]._source.MiddleInitial|hits.hits[0]._source.LastName|hits.hits[0]._source.HomeAddress|hits.hits[0]._source.HomeAddress2|hits.hits[0]._source.City|hits.hits[0]._source.State|hits.hits[0]._source.Zip|hits.hits[0]._source.Email|hits.hits[0]._source.PostBackRefId|hits.hits[0]._source.PhoneNumber|hits.hits[0]._source.SalesAmount   |hits.hits[0]._source.SiteCode|hits.hits[0]._source.TimeStamp|2020/03/25 10:30:00|hits.hits[0]._source.AccountNumber|hits.hits[0]._source.PermanentExpirationDateField|hits.hits[0]._source.CVV|


@SDPScenario4
Scenario Outline: Verify Experience API when timestamp is provided more than 15 minutes
	Given EndPoint 'https://exp-sdp-dev.ms.sandbox.mfrm.com/api/credit-decision'
	And Set the Timestamp to 'UTC' 'yyyyMMddHHmmss' -20
	When Create multiple set of SDP data's with parameterization '<ApplicationDecision>''<AccountNumber>''<PermanentExpirationDate>''<CVV>''<ApplicationKey>''<FirstName>''<MiddleInitial>''<LastName>''<HomeAddress>''<HomeAddress2/Apt/Suite>''<City>''<State>''<ZipCode>''<Emailaddress>''<PostbackReferenceID>''<PrimaryPhoneNumber>''<SaleAmount/RequestedSaleAmount>''<SiteCode>''<Timestamp>'
	When Query params with Key 'SDP' and value '$$SDPValue'
	And Header key 'client_id' value '8c7bfa646174432485a8428a656ad285'
	And Header key 'client_secret' value '5B53728899f6444AA00fC7d95307B8Ac'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Verify Value 'Request Received' present in field 'message'
	When Jest Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Jest Print Response
And Records '$.hits.hits' count present in the DB - '0'
And Verify query result contains Key 'hits.total' & Value '0'

Examples:
|QueryBody                                                                  |Table                   	|ApplicationDecision|AccountNumber   |PermanentExpirationDate|CVV|ApplicationKey|FirstName|MiddleInitial|LastName|HomeAddress    |HomeAddress2/Apt/Suite|City     |State|ZipCode|Emailaddress |PostbackReferenceID|PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode  |Timestamp        |
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678112"]}}]}}}|synchronyapplication    	|A                  |1234567898765456|2210                   |123|987678112     |ALoki    |A            |Jerry   |234 Main Street|APT 3A                |Wayne    |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234 |Set_the_Timestamp|

@Scenariopipe
Scenario Outline: Verify Experience API storing the SDP data in elastic search with postreference Id contains Pipe symbol
	Given EndPoint 'https://exp-sdp-dev.ms.sandbox.mfrm.com/api/credit-decision'
	And Set the Timestamp to 'UTC' 'yyyyMMddHHmmss' -20
	When Create multiple set of SDP data's with parameterization '<ApplicationDecision>''<AccountNumber>''<PermanentExpirationDate>''<CVV>''<ApplicationKey>''<FirstName>''<MiddleInitial>''<LastName>''<HomeAddress>''<HomeAddress2/Apt/Suite>''<City>''<State>''<ZipCode>''<Emailaddress>''ABC|32434''<PrimaryPhoneNumber>''<SaleAmount/RequestedSaleAmount>''<SiteCode>''<Timestamp>'
	When Query params with Key 'SDP' and value '$$SDPValue'
	And Header key 'client_id' value '8c7bfa646174432485a8428a656ad285'
	And Header key 'client_secret' value '5B53728899f6444AA00fC7d95307B8Ac'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Verify Value 'Request Received' present in field 'message'
	When Jest Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Jest Print Response
And Records '$.hits.hits' count present in the DB - '0'
And Verify query result contains Key 'hits.total' & Value '0'

Examples:
|QueryBody                                                                  |Table                   |ApplicationDecision|AccountNumber   |PermanentExpirationDate|CVV|ApplicationKey|FirstName|MiddleInitial|LastName|HomeAddress    |HomeAddress2/Apt/Suite|City     |State|ZipCode|Emailaddress |PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode  |Timestamp     |       
|{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678222"]}}]}}}|synchronyapplication    |A                  |1234567898765456|2210                   |123|987678222     |Andrew   |A            |Jerry   |234 Main Street|APT 3A                |Wayne    |NJ   |11245  |aj@sys.com   |7894635267        |3234.32                       |BCDE 1234 |20200326144000|

Scenario Outline: Validation for all the fields
	Given EndPoint 'https://exp-sdp-dev.ms.sandbox.mfrm.com/api/credit-decision'
	And Set the Timestamp to 'UTC' 'yyyyMMddHHmmss' -20
	When Create multiple set of SDP data's with parameterization '<ApplicationDecision>''<AccountNumber>''<PermanentExpirationDate>''<CVV>''<ApplicationKey>''<FirstName>''<MiddleInitial>''<LastName>''<HomeAddress>''<HomeAddress2/Apt/Suite>''<City>''<State>''<ZipCode>''<Emailaddress>''<PostbackReferenceID>''<PrimaryPhoneNumber>''<SaleAmount/RequestedSaleAmount>''<SiteCode>''<Timestamp>'
	When Query params with Key 'SDP' and value '$$SDPValue'
	And Header key 'client_id' value '8c7bfa646174432485a8428a656ad285'
	And Header key 'client_secret' value '5B53728899f6444AA00fC7d95307B8Ac'
	When Method 'Post' 
	And Print response
	And Statuscode '200'
	And Verify Value 'Request Received' present in field 'message'
	When Jest Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
	And Select and Verify the index '<Table>' exists
Then Get Result as String by passing Query '<QueryBody>'
And Jest Print Response
And Records '$.hits.hits' count present in the DB - '0'
And Verify query result contains Key 'hits.total' & Value '0'

Examples:
|Scenario                  |QueryBody                                                                  |Table                   	|ApplicationDecision|AccountNumber     |PermanentExpirationDate|CVV|ApplicationKey|FirstName        |MiddleInitial|LastName|HomeAddress                                    |HomeAddress2/Apt/Suite						|City                              |State|ZipCode|Emailaddress |PostbackReferenceID|PrimaryPhoneNumber|SaleAmount/RequestedSaleAmount|SiteCode   |Timestamp     |
|Invalid AppDecicion       |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678223"]}}]}}}|synchronyapplication    	|U                  |1234567898765456  |2210                   |123|987678223     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A              					    |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid AccountNo         |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678224"]}}]}}}|synchronyapplication    	|A                  |123456543256TYYU  |2210                   |123|987678224     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A             						    |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid AccNo_lenght      |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678225"]}}]}}}|synchronyapplication    	|A                  |123456543256TYYU23|2210                   |123|987678225     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A      				                |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid ExpDateFormat     |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678226"]}}]}}}|synchronyapplication    	|A                  |123456543256TYYU23|1213                   |123|987678226     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A          					        |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid ExpDatelength     |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678227"]}}]}}}|synchronyapplication    	|A                  |123456543256TYYU  |202003                 |123|987678227     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A         						        |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid CVVLenth          |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678228"]}}]}}}|synchronyapplication    	|A                  |123456543256TYYU23|2210                   |1234|987678228    |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A          					        |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid CVVFormat         |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678229"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |12A|987678229     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A    						            |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid FirstName         |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678230"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678230     |ALokiloihytresrty|A            |Jerry   |234 Main Street                                |APT 3A           			    		    |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid MiddleInitial     |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678231"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678231     |ALoki            |AD           |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid Invalid LastName  |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678232"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678232     |ALoki            |A            |Jer-ry  |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid Address           |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678233"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678233     |ALoki            |A            |Jerry   |234 Main Streetwef ewfwfefwefwefwwefwefwefccxzc|APT 3A                                      |Wayne                             |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid City              |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678235"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678235     |ALoki            |A            |Jerry   |234 Main Street								   |APT 3A              					    |Waynejurjkrtyuiokmbdgrt egeged    |NJ   |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid State             |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678236"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678236     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJY  |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid ZipCode           |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678237"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678237     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |M12K5  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid ZipCodeLength     |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678238"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678238     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |112456 |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid EmailAddress      |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678239"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678239     |ALoki            |A            |Jerry   |234 Main Street						    	   |APT 3A         						        |Wayne                             |NJ   |11245  |aj@sys.com.  |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid EmailAdd_lenght   |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678240"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678240     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |11245  |ajsyscom     |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid PhoneNo           |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678241"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678241     |ALoki            |A            |Jerry   |234 Main Street								   |APT 3A              					    |Waynejurjkrtyuiokmbdgrt egeged    |NJ   |11245  |aj@sys.com   |12345WEED          |789463526756A     |3234.32                       |BCDE 1234  |20200324183500|
|Invalid SiteCode          |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678242"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678242     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJY  |11245  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE31234TR|20200324183500|
|Invalid ZipCode           |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678243"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678243     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |M12K5  |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|
|Invalid TimeStamp         |{"query":{"bool": {"must": [{"terms":{"ApplicationKey": ["987678244"]}}]}}}|synchronyapplication    	|A                  |1234567898765456  |2210                   |123|987678244     |ALoki            |A            |Jerry   |234 Main Street                                |APT 3A                                      |Wayne                             |NJ   |112456 |aj@sys.com   |12345WEED          |7894635267        |3234.32                       |BCDE 1234  |20200324183500|











