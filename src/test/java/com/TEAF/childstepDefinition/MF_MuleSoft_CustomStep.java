package com.TEAF.childstepDefinition;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.collection.IsEmptyCollection;

import org.hamcrest.collection.IsEmptyCollection;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.hamcrest.number.OrderingComparison.lessThan;

import static org.hamcrest.MatcherAssert.assertThat;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.mail.Store;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.util.FileUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.Resources.ReadingEmail;
import com.Resources.ReadingFailureEmail;
import com.Resources.ReadingSuccessEmail;
import com.Resources.SDPEncryptDecrypt;
import com.TEAF.framework.ElasticSearchJestUtility;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.TEAF.framework.StepBase;
import com.TEAF.stepDefinitions.CommonSteps;
import com.TEAF.stepDefinitions.RestAssuredSteps;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import cucumber.runtime.junit.Assertions;
import io.restassured.internal.assertion.Assertion;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONValue;

public class MF_MuleSoft_CustomStep {
	
	public static RequestSpecification spec = given().when();
	static JSONObject RequestParameters = new JSONObject();
	public static String Path;
	private static final String Null = null;

	private static final ResponseOptions<Response> GetResponse = null;
	private static final Object Customer_Code__c = null;
	RestAssuredUtility Rest = new RestAssuredUtility();
	static Logger log = Logger.getLogger(MF_MuleSoft_CustomStep.class.getName());

	
	SDPEncryptDecrypt sdpdata = new SDPEncryptDecrypt();
	   
    @When("^Create multiple set of SDP data's with parameterization '(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)''(.*)'$")
    public void Create_multiple_set_of_SDP_datas_with_parameterization(String param1, String param2,String param3, String param4,String param5, String param6,String param7, String param8,String param9, String param10,String param11, String param12,String param13, String param14,String param15, String param16,String param17, String param18,String param19) throws Throwable {
        String sdpvalue = sdpdata.shouldGenerateSampleData(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19);
        HashMapContainer.add("$$SDPValue", sdpvalue);
    }
   
    public void kibana() {
//        JestClientFactory factory = new JestClientFactory();
//        factory.setHttpClientConfig(new HttpClientConfig.Builder("https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/")
//                        .multiThreaded(true).build());
//        JestClient client = factory.getObject();
//       
//        boolean indexExists = client.execute(new IndicesExists.Builder("synchronyapplication").build()).isSucceeded();
//        System.out.println(indexExists);
//            String query = "{\r\n" +
//                    "  \"query\":{\r\n" +
//                    "    \"bool\": {\r\n" +
//                    "           \"must\": [\r\n" +
//                    "             {\r\n" +
//                    "           \"terms\":{\r\n" +
//                    "              \"ApplicationKey\": [\r\n" +
//                    "                  \"19830732\"\r\n" +
//                    "                ]\r\n" +
//                    "               \r\n" +
//                    "               }\r\n" +
//                    "             }\r\n" +
//                    "      ]\r\n" +
//                    "    }\r\n" +
//                    "  }}";
//
//
//
//        Search.Builder searchBuilder = new Search.Builder(query).addIndex("synchronyapplication");
//        SearchResult result = client.execute(searchBuilder.build());
//        System.out.println(result.getJsonString());
    }
    @When("^EndPoint '(.*)' with random postbackRefID$")
    public void Endpoint_with_random_postbackRefID(String endpoint) {
    	String randomPostBackrefernceID =RandomStringUtils.randomAlphanumeric(10);
        String endPointWithpostbackRefID = endpoint + randomPostBackrefernceID;
        System.out.println("--------------------"+endPointWithpostbackRefID);
        RestAssuredSteps.endPoint(endPointWithpostbackRefID);
        HashMapContainer.add("$$RandomPostBackrefernceID", randomPostBackrefernceID);
    }
	
	@When("^Get Servicing DC Number from Elastic Search or warehouse list$")
	public void getmf_id_servicengDCno() throws Exception {
		String sdc = HashMapContainer.get("$$ServicingDCNo");

		if (sdc.length() > 2) {

			List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(),
					"$.." + sdc);
			String data = read.get(0);
			HashMapContainer.add("$$WareHouseC", data);
			
		}
	}
	
	
	@When("^I verify the Failure email is received and read the body content$")
	public void mf_i_verify_the_Failure_email_body_content() {
		ReadingFailureEmail.getemailRead();
	}

	@When("^I verify the email is received and read the body content$")
	public void i_verify_the_email_body_content() {
		ReadingEmail.getemailRead();
	}

	@When("^I verify the Proccess completed email is received and read the body content$")
	public void i_verify_the_Process_completed_email_body_content() {
		ReadingSuccessEmail.getemailRead();
	}

	
	static List<Object> storeArray = new ArrayList<Object>();
	static List<Object> customerCode = new ArrayList<Object>();

	@When("^Get the key '(.*)' from response and store it as array$")
	public void get_the_key_from_response_store_it_as_array(String key) {
		 storeArray = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
		System.out.println("================ Values Stord in Array ==================");
		System.out.println(storeArray);
		
		System.out.println("================ Values Stord in last Array ==================");
		System.out.println(storeArray.get(storeArray.size()-1));
		String LastProcessedCustomerCode =(String) storeArray.get(storeArray.size()-1);
		HashMapContainer.add("$$LastProcessedCustomerCode", LastProcessedCustomerCode);
//		Assert.assertEquals(Integer.parseInt(data), read.size());
	}

	@When("^Get the datas from elastic search Index and Upsert it in the Salesforce$")
	public static void get_customer_code_From_mentioned_master_code() throws Exception {
		List <Object> arr1 = new ArrayList <Object>();
		RestAssuredSteps rs = new RestAssuredSteps();
//		String jsonString1 = null;
		String LastOrderDate = null;
		List <SfdcRequestPayload> sfdclist =new ArrayList<SfdcRequestPayload>(); 
		for (int i =0;i < storeArray.size(); i++) {
			System.out.println("===========Starting of Customer Code============="+ storeArray.size()+ " = "+ i);
			RestAssuredSteps.endPoint(
					"http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/crossref");
			rs.content_type("application/json");
			rs.header_key_value("Content-Type", "application/json");
			rs.updatequeryParams("flowType", "CUST_UPSERT");
			rs.updatequeryParams("childId", String.valueOf(storeArray.get(i)));
			rs.Method("Get");
			rs.print_response();
			rs.statuscode(200);
			customerCode = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$..customerCode");
			System.out.println("=================Customer Code===============");
			
			System.out.println(customerCode);
			System.out.println("***************Size*******************" + customerCode.size());
			String LastProcessedCustomerCode = (String) customerCode.get(customerCode.size()-1);
			HashMapContainer.add("$$LastProcessedCustomerCode", LastProcessedCustomerCode);
		
				RestAssuredSteps.endPoint(
						"http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/customerSalesOrder/custCodes");
				rs.content_type("application/json");
				rs.header_key_value("Content-Type", "application/json");
				
				List<Map<String, String>> cusCodeCollection = new ArrayList<Map<String, String>>();
			
				Map<String, String> parentcusCode = new HashMap<String, String>();
				parentcusCode.put("customerCodes", "");
				parentcusCode.put("ultimateMasterCode", String.valueOf(storeArray.get(i)));
				
				cusCodeCollection.add(parentcusCode);
				for (int l = 0; l < customerCode.size(); l++) {
				Map<String, String> childCusCode = new HashMap<String, String>();
				childCusCode.put("customerCodes", "");
				childCusCode.put("ultimateMasterCode", String.valueOf(customerCode.get(l)));
			
				cusCodeCollection.add(childCusCode);
				}
				Map<String, List<Map<String, String>>> inputMap = new HashMap<String, List<Map<String, String>>>();
				inputMap.put("inputMap", cusCodeCollection);
				
				String jsonString = org.json.simple.JSONValue.toJSONString(inputMap);
				
				RestAssuredSteps.request_body_json_object(jsonString);
				
				rs.Method("Post");
				rs.print_response();
				rs.statuscode(201);
			
			//TODO - validate all total salesamount in DB for all the master and Child records
				List read = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$..OrderTotalAmount");
				System.out.println(read);
			    int size1 = read.size();
			    System.out.println("**********************************"+size1);
			    double totalOrdervalue = 0;
			    for (int k=0;k<size1;k++) {
			    	
			    	double count =  (Double) read.get(k);
			    	 totalOrdervalue = totalOrdervalue + count;
			    	 
			    }DecimalFormat df = new DecimalFormat("#.##");     
                totalOrdervalue = Double.valueOf(df.format(totalOrdervalue));
				
				List<String> read1 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$..LastOrderDate");
				System.out.println("---------------------------------" + read1);
                 int datesize = read1.size();
//                 List <String> valid = null;
                 List<String> arr11 = new ArrayList<String>();
                 for(int m=0;m<datesize;m++) {
                	 if(read1.get(m)!=null) {
                		 System.out.println("--------------------------"+String.valueOf(read1.get(m)));
                		 arr11.add(read1.get(m));
                		 
                	 }else {
                		 System.out.println("Null value is present in response LastOrderDate");
                	 }
                 }
                 
                String finaldate = null;
				String latest= null;
				try {
					
					latest = Collections.max(arr11);
					SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date = format.parse(latest);
				    finaldate= date.toString();
					SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					 LastOrderDate = sdf1.format(date);
					log.info(LastOrderDate);
					HashMapContainer.add("$$LastUpdatedate", LastOrderDate);
					
				} catch (NullPointerException e) {
					log.warning("Null value is present in the Last Orderdate");
				}
				log.info(latest);
				
				HashMapContainer.add("$$latesttimestamp", latest);
				int timestampsize= HashMapContainer.get("$$latesttimestamp").length();
				System.out.println();
//				log.info(timestampsize);
				
				List read2 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), "$..OrderCount");
				System.out.println(read2);
			    int size = read2.size();
			    int total = 0;
			    for (int n=0;n<size;n++) {
			    	
			    	int count = (Integer) read2.get(n);
			    	total = total+ count;
			    }
			    int totalordercount = total;
			    System.out.println("_____________________________________________________"+totalordercount);
			
			RestAssuredSteps.endPoint(
					"http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert");
			rs.content_type("application/json");
			rs.header_key_value("Content-Type", "application/json");
			
			SfdcRequestPayload sfdc=new SfdcRequestPayload();
			SFDCpayloadlist sfdcpl = new SFDCpayloadlist();
			sfdc.setCustomer_Code__c(String.valueOf(storeArray.get(i)));
			sfdc.setLast_Order_Date__c(LastOrderDate);
			sfdc.setLifeTime_Value__c(totalOrdervalue);
			sfdc.setNumber_of_Orders__c(totalordercount);
//			String jsonString1= gson.toJson(sfdc);
			sfdclist.add(sfdc);
//			String jsonString1 = org.json.simple.JSONValue.toJSONString(sfdc);
//            System.out.println("************************************"+jsonString1);
           
//            List <String> arr = null;
//            sfdcpl.setRequestpl(sfdc);
//            arr1.add(jsonString1);
          
            }
		Gson gson=new Gson();
       System.out.println("**********************"+arr1);
       String jsonString1 = gson.toJson(sfdclist);
       
        rs.request_body_json_object(jsonString1);
       log.info(jsonString1);
		
		rs.Method("Post");
		rs.print_response();
		rs.statuscode(200);	
	}
	
	@Given("Set the Timestamp to {string} {string} {int}")
public void get__FormattedTime_byTimeZone_And_TimeOffsetbyMinutes(String timeZone, String timeFormat, int timeOffsetfByMinutes) {
		
		DateFormat tFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date date = new Date();
		String currentTime =  tFormat.format(date);
		System.out.println("Current Local Time: "+currentTime);
		
		
		tFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
		String Time = tFormat.format(date);
		System.out.println(timeZone+" Time: "+Time);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime datetime = LocalDateTime.parse(Time,formatter);
		
		if(timeOffsetfByMinutes<0) {
			System.out.println("Subtracting Time");
			datetime=datetime.minusMinutes(-timeOffsetfByMinutes);
		}else {
			System.out.println("Adding Time");
			datetime=datetime.plusMinutes(timeOffsetfByMinutes);
		}
		
		DateTimeFormatter finalformat = DateTimeFormatter.ofPattern(timeFormat);
//		return datetime.format(finalformat);
		String timestamp = datetime.format(finalformat);
		HashMapContainer.add("$$Timestamp", timestamp);
		System.out.println("******************************************"+timestamp);
	}

	@When("^Get the response for Elastic Search Index and store the keys$")
	public void get_response_for_elastic_search_index_and_store_the_keys() throws Exception {
		RestAssuredSteps steps = new RestAssuredSteps();
		MF_MuleSoft_CustomStep Csteps = new MF_MuleSoft_CustomStep();
		String asString = RestAssuredSteps.getResponse().getBody().asString();
		List read = JsonPath.read(asString, "$.[*]Id");
		List<Map<String, Comparable>> arr = new ArrayList<Map<String, Comparable>>();

		Map<String, Object> parentObj = new LinkedHashMap<String, Object>();
		parentObj.put("sObjectType", "Account");
		parentObj.put("flowType", "Store");
		parentObj.put("primarykey", "MFRM_Oracle_Record_ID__c");
		parentObj.put("upsertPayload", arr);
		CustomerUpSert cs = new CustomerUpSert();

		for (int i = 0; i < read.size(); i++) {
			Object am = JsonPath.read(asString, "$.[" + i + "]AreaManager");
			if (String.valueOf(am).length() == 0) {
				cs.setArea_Manager__c(String.valueOf(am));
			}

			Object cc = JsonPath.read(asString, "$.[" + i + "]Brand");
			cs.setChannel__c(String.valueOf(cc));

			Object cld = JsonPath.read(asString, "$.[" + i + "]ClosureDate");
			String cd = String.valueOf(cld);
			if (!cd.equals("1900/01/01 00:00:00") && !cd.equals("null")) {
				SimpleDateFormat cdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = cdf.parse(cd);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-5:00");
				String dateString = sdf.format(date);
				System.out.println("Date in the format of E, dd/MMM/yyyy HH:mm:ss z : " + dateString);
				cs.setClosed_Date__c(dateString);
			} else {
				cs.setClosed_Date__c(Null);
			}

			Object dm = JsonPath.read(asString, "$.[" + i + "]DistrictManager");
			cs.setDistrict_Manager__c(String.valueOf(dm));

			Object fx = JsonPath.read(asString, "$.[" + i + "]Fax");
			String fax = String.valueOf(fx);
			if (fax.matches("[0-9]+") && fax.length() > 2) {
				cs.setFax(fax);
			} else {
				cs.setFax("");
			}

			Object name = JsonPath.read(asString, "$.[" + i + "]Name");
			cs.setName(String.valueOf(name));
			Object openDate = JsonPath.read(asString, "$.[" + i + "]OpenDate");
			String ud = String.valueOf(openDate);
			if (!ud.equals("1900/01/01 00:00:00") && !ud.equals("null")) {
				SimpleDateFormat udf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = udf.parse(ud);
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-5:00");
				String dateString1 = sdf1.format(date);
				System.out.println("Date in the format of E, dd/MMM/yyyy HH:mm:ss z : " + dateString1);
				cs.setOpen_Date__c(dateString1);
			} else {
				cs.setOpen_Date__c(Null);

			}

			Object tp = JsonPath.read(asString, "$.[" + i + "]Type");
			String type = String.valueOf(tp);
			if (type.endsWith("whse")) {
				cs.setRecordTypeId("012G00000017CrPIAU");
			} else {
				cs.setRecordTypeId("012G00000017CrNIAU");
			}

			Object sdc = JsonPath.read(asString, "$.[" + i + "]ServicingDCNo");
			cs.setWarehouse_Code__c(String.valueOf(sdc));
			System.out.println("****************__________________________"+String.valueOf(sdc));
			String sdc1 =String.valueOf(sdc);
//			HashMapContainer.add("$$ServicingDCNo", String.valueOf(sdc));
			
			steps.endPoint("http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/warehouseList");
			steps.content_type("application/json");
			steps.header_key_value("Content-Type", "application/json");
			steps.Method("Get");
			steps.print_response();
			if (sdc1.length() > 2) {

				List<String> read1 = com.jayway.jsonpath.JsonPath.read(steps.getResponse().asString(),
						"$.." + sdc1);
				String data = read1.get(0);
				HashMapContainer.add("$$WareHouseC", data);
				
			}
			
			String whc = HashMapContainer.get("$$WareHouseC");
			if( whc != null ) {
			cs.setWarehouse__c(whc);
			}else {
				cs.setWarehouse__c("");
			}
			
			
			if (type.endsWith("ecom")) {
				cs.setWeb_Store__c(true);
			} else {
				cs.setWeb_Store__c(false);
			}
			
			Object read2 = JsonPath.read(asString, "$.[" + i + "]RegionalVP");
			
			System.out.println(read2);
cs.setRegional_Vice_President__c(JsonPath.read(asString, "$.[" + i + "]RegionalVP").toString());
//			cs.setRegional_Vice_President__c(String.valueOf(JsonPath.read(asString, "$.[" + i + "]RegionalVP")));
			cs.setSales_Market__c(JsonPath.read(asString, "$.[" + i + "]Market").toString());
			cs.setShippingCity(JsonPath.read(asString, "$.[" + i + "]City").toString());

			cs.setShippingCountry("US");

			cs.setShippingPostalCode(JsonPath.read(asString, "$.[" + i + "]Zip").toString());
			cs.setShippingState(JsonPath.read(asString, "$.[" + i + "]StateCode").toString());
			cs.setShippingStreet(JsonPath.read(asString, "$.[" + i + "]Address").toString());
			cs.setStore_Region__c(JsonPath.read(asString, "$.[" + i + "]Region").toString());

			cs.setStore_Warehouse_Status_Code__c(JsonPath.read(asString, "$.[" + i + "]StatusCode").toString());

			String id = JsonPath.read(asString, "$.[" + i + "]Id").toString();

			if (!id.matches("[0-9]+")) {
				String substring = id.substring(3);
				cs.setMFRM_Oracle_Record_ID__c(substring);
			} else {
				cs.setMFRM_Oracle_Record_ID__c(id);

			}

			Object ph = JsonPath.read(asString, "$.[" + i + "]Phone");
			String phone = String.valueOf(ph);
			if (phone.matches("[0-9]+")) {
				cs.setPhone(phone);
			} else {
				cs.setPhone("");
			}

			String email = JsonPath.read(asString, "$.[" + i + "]Email").toString();
			if (email.contains("@") && email.contains(".com")) {
				cs.setStore_Warehouse_Email__c(email);
			} else {
				cs.setStore_Warehouse_Email__c("");
			}

			cs.setDistrict__c(JsonPath.read(asString, "$.[" + i + "]District").toString());
//			cs.setHours_of_Operations__c(String.valueOf(JsonPath.read(asString, "$.[" + i + "]StoreHours")));
			cs.setDMA__c(JsonPath.read(asString, "$.[" + i + "]DMA").toString());

			cs.setDivisional_Vice_President__c(JsonPath.read(asString, "$.[" + i + "]SalesDivisionalVP").toString());

			Map<String, Comparable> childobj = new LinkedHashMap<String, Comparable>();
			childobj.put("Area_Manager__c", cs.getArea_Manager__c());
			childobj.put("Channel__c", cs.getChannel__c());
			childobj.put("Closed_Date__c", cs.getClosed_Date__c());
			childobj.put("District_Manager__c", cs.getDistrict_Manager__c());
			childobj.put("Fax", cs.getFax());
			childobj.put("Name", cs.getName());
			childobj.put("Open_Date__c", cs.getOpen_Date__c());
			childobj.put("RecordTypeId", cs.getRecordTypeId());
			childobj.put("Warehouse_Code__c", cs.getWarehouse_Code__c());
			childobj.put("Warehouse__c", cs.getWarehouse__c());
			childobj.put("Web_Store__c", cs.getWeb_Store__c());
			childobj.put("Regional_Vice_President__c", cs.getRegional_Vice_President__c());
			childobj.put("Sales_Market__c", cs.getSales_Market__c());
			childobj.put("ShippingCity", cs.getShippingCity());
			childobj.put("ShippingCountry", cs.getShippingCountry());
			childobj.put("ShippingPostalCode", cs.getShippingPostalCode());
			childobj.put("ShippingState", cs.getShippingState());
			childobj.put("ShippingStreet", cs.getShippingStreet());
			childobj.put("Store_Region__c", cs.getStore_Region__c());
			childobj.put("Store_Warehouse_Email__c", cs.getStore_Warehouse_Email__c());
			childobj.put("Store_Warehouse_Status_Code__c", cs.getStore_Warehouse_Status_Code__c());
			childobj.put("MFRM_Oracle_Record_ID__c", cs.getMFRM_Oracle_Record_ID__c());
			childobj.put("Phone", cs.getPhone());
			childobj.put("District__c", cs.getDistrict__c());
			childobj.put("DMA__c", cs.getDMA__c());
			childobj.put("Divisional_Vice_President__c", cs.getDivisional_Vice_President__c());
//			childobj.put("Hours_of_Operations__c", "");

			arr.add(childobj);
		}
		String jsonText = JSONValue.toJSONString(parentObj);
		HashMapContainer.add("$$StoreUpsertData", jsonText);
		String Response = HashMapContainer.get("$$StoreUpsertData");
		 File f = new File("C:\\Users\\User\\Downloads\\PremRC-matressfirm_testautomation-668ce572de29\\src\\test\\java\\com\\TEAF\\json\\StoreUPsert.json");
         boolean Exists = f.exists();
         if (Exists) {
                System.out.println("Printing response to File");
                this.write(f, Response);
         } else {
                f.createNewFile();
                System.out.println("New file created");
                this.write(f, Response);
         }
        
	}


	private void write(File f, String response) {
		try {
            FileOutputStream f1 = new FileOutputStream(f);
           OutputStreamWriter o = new OutputStreamWriter(f1);
            o.write(response);
//            writeObject(Response);
            System.out.println(response);
            o.close();
            f1.close();
     } catch (FileNotFoundException e) {
            System.out.println("File not found");
     } catch (IOException e) {
            System.out.println("Error initializing stream");
     }
}
		
	

	@When("^Get the response for Elastic Search Index and store the Product keys$")
	public static void get_response_for_elastic_search_index_and_store_the__Product_keys() throws ParseException {
		String response_pro = RestAssuredSteps.getResponse().getBody().asString();
		List read = JsonPath.read(response_pro, "$.[*]ITM_CD");
		List<Map<String, Comparable>> arr1 = new ArrayList<Map<String, Comparable>>();
//		Gson  gson = new Gson();
		Map<String, Object> parentObj1 = new LinkedHashMap<String, Object>();
		parentObj1.put("sObjectType", "Account");
		parentObj1.put("flowType", "Store");
		parentObj1.put("primarykey", "MFRM_Oracle_Record_ID__c");
		parentObj1.put("upsertPayload", arr1);
		ProductUpsert ps = new ProductUpsert();

		for (int i = 0; i < read.size(); i++) {
			Object name = JsonPath.read(response_pro, "$.[" + i + "]ITM_CD");
			ps.setName(String.valueOf(name));

			Object des = JsonPath.read(response_pro, "$.[" + i + "]DES");
			ps.setDescription(String.valueOf(des));

			Object act = JsonPath.read(response_pro, "$.[" + i + "]Active");
			String type = String.valueOf(act);
			if (type.equals("Y")) {
				ps.setIsActive(true);
			} else {
				ps.setIsActive(false);
			}

			Object brand = JsonPath.read(response_pro, "$.[" + i + "]Brand");
			ps.setBrand__c(String.valueOf(brand));

			Object ptype = JsonPath.read(response_pro, "$.[" + i + "]ProductType");
			ps.setProduct_Type__c(String.valueOf(ptype));

			Object size = JsonPath.read(response_pro, "$.[" + i + "]SIZ");
			ps.setSize__c(String.valueOf(size));

			Object vsn = JsonPath.read(response_pro, "$.[" + i + "]ITM_CD");
			ps.setVSN__c(String.valueOf(vsn));

			String name1 = JsonPath.read(response_pro, "$.[" + i + "]VE_CD").toString();
			String name2 = JsonPath.read(response_pro, "$.[" + i + "]Vendor").toString();
			String name3 = name1 + " " + name2;
			System.out.println(name3);
			StringBuilder builder = new StringBuilder(name3);
			builder.setLength(38);
			String restName = builder.toString().trim();
//		System.out.println(name.substring(0, stringlimit).trim());
			System.out.println(restName);
			if (restName != null) {

				ps.setVendor__c(restName);
			} else {
				ps.setVendor__c("");
			}

			Object warranty = JsonPath.read(response_pro, "$.[" + i + "]WarrantyPeriod");
			ps.setWarranty__c(String.valueOf(warranty));

//		Object corecost = JsonPath.read(response_pro, "$.[" + i + "]CoreCost");
//		ps.setCore_Price__c(String.valueOf(corecost));

			String path  = "$.[" + i + "]CoreCost";
			String cost = JsonPath.read(response_pro, path);
		
			System.out.println(cost);
			if (cost != null) {

				ps.setCore_Price__c(cost);
			} else {
//			cost.JSONObject.NULL;
				ps.setCore_Price__c(Null);
			}

//		Object msp = JsonPath.read(response_pro, "$.[" + i + "]MerchSalesPrice");
//		ps.setMerch_Sales_Price__c(String.valueOf(msp));
			String MerchPrice = JsonPath.read(response_pro, "$.[" + i + "]MerchSalesPrice");
			System.out.println(MerchPrice);
			if (cost != "null") {

				ps.setMerch_Sales_Price__c(MerchPrice);
			} else {
//			cost.JSONObject.NULL;
				ps.setMerch_Sales_Price__c(Null);
			}

			Object domain = JsonPath.read(response_pro, "$.[" + i + "]Domain");
			ps.setDomain__c(String.valueOf(domain));

			Object classname = JsonPath.read(response_pro, "$.[" + i + "]Class");
			ps.setClass__c(String.valueOf(classname));

//		Object segment = JsonPath.read(response_pro, "$.[" + i + "]Segment");
//		ps.setWarranty__c(String.valueOf(segment));

			String segment = JsonPath.read(response_pro, "$.[" + i + "]Segment");
			System.out.println(segment);
			if (segment != null) {

				ps.setSegment__c(segment);
			} else {
				ps.setSegment__c("");
			}

			String box = JsonPath.read(response_pro, "$.[" + i + "]Box");
			System.out.println(box);
			if (box != null) {

				ps.setBox__c(box);
			} else {
				ps.setBox__c("");
			}

//		Object disposition = JsonPath.read(response_pro, "$.[" + i + "]Disposition");
//		ps.setWarranty__c(String.valueOf(disposition));

			String dis = JsonPath.read(response_pro, "$.[" + i + "]Disposition");
			System.out.println(dis);
			if (dis != null) {

				ps.setDisposition__c(dis);
			} else {
				ps.setDisposition__c("");
			}

			Map<String, Comparable> childobj1 = new LinkedHashMap<String, Comparable>();
			childobj1.put("Name", ps.getName());
			childobj1.put("Description", ps.getDescription());
			childobj1.put("IsActive", ps.getIsActive());
			childobj1.put("Brand__c", ps.getBrand__c());
			childobj1.put("Product_Type__c", ps.getProduct_Type__c());
			childobj1.put("Size__c", ps.getSize__c());
			childobj1.put("VSN__c", ps.getVSN__c());
			childobj1.put("Vendor__c", ps.getVendor__c());
			childobj1.put("Warranty__c", ps.getWarranty__c());
			childobj1.put("Core_Price__c", ps.getCore_Price__c());
			childobj1.put("Merch_Sales_Price__c", ps.getMerch_Sales_Price__c());
			childobj1.put("Domain__c", ps.getDomain__c());
			childobj1.put("Class__c", ps.getClass__c());
			childobj1.put("Segment__c", ps.getSegment__c());
			childobj1.put("Box__c", ps.getBox__c());
			childobj1.put("Disposition__c", ps.getDisposition__c());

			arr1.add(childobj1);

		}
//		String json = gson.toJson(parentObj1);
//		String json = new Gson().toJson(parentObj1);
		String jsonText1 = org.json.simple.JSONValue.toJSONString(parentObj1);
		HashMapContainer.add("$$ProductUpsertData", jsonText1);
		System.out.println(
				"=======================-----------------------------ProductUpsert------------------------============================");
		System.out.println(jsonText1);
	}

	
	
	@When("^Get the response for Elastic Search Index and store the keys for missing contacts$")
	public void Get_the_response_for_Elastic_Search_Index_and_store_the_keys_for_missing_contacts() throws ParseException{
	    
        String asString = RestAssuredSteps.getResponse().getBody().asString();
        List read = JsonPath.read(asString, "$.[*]STOREID");
        List<Map<String, Comparable>> arr = new ArrayList<Map<String, Comparable>>();

 

        Map<String, Object> parentObj = new LinkedHashMap<String, Object>();
        parentObj.put("sObjectType", "Account");
        parentObj.put("flowType", "missing_cust");
        parentObj.put("primarykey", "MFRM_Oracle_Record_ID__c");
        parentObj.put("upsertPayload", arr);
        
        MissingCustomerUpsert MCU = new MissingCustomerUpsert();
        
        for (int i = 0; i < read.size(); i++) {
            
            Object SI = JsonPath.read(asString, "$.[" + i + "]STOREID");
            if (String.valueOf(SI).length() != 0) {
                MCU.setAccountId(String.valueOf(SI));
            }else {
                MCU.setAccountId("");
            }
            
            HashMapContainer.add("$$MCUStoreID", (String) SI);
            
            Object LN = JsonPath.read(asString, "$.[" + i + "]LNAME");
            if (String.valueOf(LN).length() != 0) {
                MCU.setLastName(String.valueOf(LN));
            }else {
                MCU.setLastName("X");
            }

            MCU.setFirstName(String.valueOf(JsonPath.read(asString, "$.[" + i + "]FNAME")));
            
            MCU.setRecordTypeId("012G00000017CrRIAU");        //hardcoded
            
    //        String Add = trim(("$.[" + i + "]ADDR1" + " " + "$.[" + i + "]ADDR2") replace /\s{2,}/ with " ")
            
            String name1 = String.valueOf(JsonPath.read(asString, "$.[" + i + "]ADDR1"));
            String name2 = String.valueOf(JsonPath.read(asString, "$.[" + i + "]ADDR2"));
            String name3 = name1 + " " + name2;
            System.out.println(name3);
            MCU.setOtherStreet(name3);
            
            MCU.setOtherCity(String.valueOf(JsonPath.read(asString, "$.[" + i + "]CITY")));
            
            MCU.setOtherState(String.valueOf(JsonPath.read(asString, "$.[" + i + "]ST_CD")));
            
            MCU.setOtherPostalCode(String.valueOf(JsonPath.read(asString, "$.[" + i + "]ZIP_CD")));
            
            Object ph = JsonPath.read(asString, "$.[" + i + "]HOME_PHONE");
            String phone = String.valueOf(ph);
            if (phone.matches("[0-9]+")) {
                MCU.setHomePhone(phone);
            } else {
                MCU.setHomePhone("");
            }
            
            Object ph1 = JsonPath.read(asString, "$.[" + i + "]ISMOBILEPHONE");
            String phone1 = String.valueOf(ph1);
            if (phone1 == "1") {
                MCU.setMobilePhone(phone);
            } else {
                MCU.setMobilePhone("");
            }
            
            Object dnc = JsonPath.read(asString, "$.[" + i + "]DO_NOT_CONTACT");
            String dnct = String.valueOf(dnc);
            if (dnct == "1") {
                MCU.setHasOptedOutOfEmail(true);
            } else {
                MCU.setHasOptedOutOfEmail(false);
            }
            
            MCU.setCustomer_Code__c(String.valueOf(JsonPath.read(asString, "$.[" + i + "]CUST_CD")));
            
            Map<String, Comparable> childobj = new LinkedHashMap<String, Comparable>();
            childobj.put("AccountId", MCU.getAccountId());
            childobj.put("LastName", MCU.getLastName());
            childobj.put("FirstName", MCU.getFirstName());
            childobj.put("RecordTypeId", MCU.getRecordTypeId());
            childobj.put("OtherStreet", MCU.getOtherStreet());
            childobj.put("OtherCity", MCU.getOtherCity());
            childobj.put("OtherState", MCU.getOtherState());
            childobj.put("OtherPostalCode", MCU.getOtherPostalCode());
            childobj.put("OtherCountry", MCU.getOtherCountry());
            childobj.put("HomePhone", MCU.getHomePhone());
            childobj.put("MobilePhone", MCU.getMobilePhone());
            childobj.put("Email", MCU.getEmail());
            childobj.put("HasOptedOutOfEmail", MCU.getHasOptedOutOfEmail());
            childobj.put("Customer_Code__c", MCU.getCustomer_Code__c());
            childobj.put("invalid_email_address__c", MCU.getInvalid_email_address__c());
            
            arr.add(childobj);
        }
        String jsonText = JSONValue.toJSONString(parentObj);
        HashMapContainer.add("$$MissingCustomerData", jsonText);
    }
    
	@When("^Update the Missing customer data to SFDC Upsert$")
	public void update_the_missing_customer_upsert_data_to_sfdc() throws IOException {

		RestAssuredSteps.request_body_json_object(HashMapContainer.get("$$MissingCustomerData"));

	}
	
	
	@And("^Retrieve data for all customerCodes and store in array list$")
	public void Retrieve_data_for_all_customerCodes() {

		String asString = RestAssuredSteps.getResponse().getBody().asString();
		List arr = new ArrayList();
		arr = JsonPath.read(asString, "$.hits.hits[*]._source.customerCode");
		int size = arr.size();

		List<Map<String, String>> arr1 = new ArrayList<Map<String, String>>();

		Map<String, String> parentObj = new LinkedHashMap<String, String>();
		parentObj.put("inputMap", "arr1");

		ContactMerge CM = new ContactMerge();

		for (int i = 0; i <= size; i++) {

			Object cc = JsonPath.read(asString, "$.inputMap[" + i + "].ultimateMasterCode");
			CM.setUltimateMasterCode(String.valueOf(cc));

			Map<String, String> childobj = new LinkedHashMap<String, String>();
			childobj.put("customerCodes", "");
			childobj.put("ultimateMasterCode", CM.getUltimateMasterCode());

			arr1.add(childobj);
		}

		String jsonText = JSONValue.toJSONString(parentObj);
		HashMapContainer.add("$$ContactMergeData", jsonText);

	}

	@When("^Update the Store upsert data to SFDC Upsert$")
	public void update_the_customer_upsert_data_to_sfdc() throws Exception {
//	RestAssuredSteps.update_String_data_request("StoreUPsert", "info[0].last_processedkey", "$$ModifiedID");
		RestAssuredSteps.request_body_json_object(HashMapContainer.get("$$StoreUpsertData"));

	}

	@When("^Update the Product upsert data to SFDC Upsert$")
	public void update_the_upsert_data_to_product_sfdc() throws IOException {

		RestAssuredSteps.request_body_json_object(HashMapContainer.get("$$ProductUpsertData"));
	}

	@Then("Verify the response body is null")
	public boolean verify_response_body_null() {

		String Actual = RestAssuredSteps.getResponse().getBody().asString();
		assertEquals("null", Actual);
		return false;
	}

	@Given("Query param key {string} value {string}")
	public void query_param_key_value(String string, String string2) {
		spec = spec.queryParams(string, string2);
	}

	@Given("Query Param name {string} value {string}")
	public void param_name_value(String string, String string2) {
		spec = given().queryParam(string, string2);
	}

	@Then("Verify JSONPath {string} contains the Prefix {string}")
	public void match_JSONPath_containsID(String path, String Expected) {
		if (path.length() > 0) {
			Object Actual = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), path);
			System.out.println("Value " + String.valueOf(Actual));
			assertEquals(" Json Path Value Check ", Expected, String.valueOf(Actual).substring(0, 3));
		}
	}

	@Then("Verify the response body is empty")
	public void verify_response_body_Empty() {

		String Actual = RestAssuredSteps.getResponse().getBody().asString();

		Actual = Actual.replaceAll("\\s", "");

		assertEquals("{}", Actual);
		
	}


	@When("^Verify key '(.*)' count present in the response orders- '(.*)'$")
	public void key_count_present_in_the_response_orders(String key, String count) {

		String data = HashMapContainer.get(count);
		List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
		Assert.assertEquals(Integer.parseInt(data), read.size());
	}

	@When("^Verify key '(.*)' count present in the List response orders- '(.*)'$")
	public void key_count_present_in_the_response_orders_list(String key, String count) {

		String data = HashMapContainer.get(count);
		List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
		Assert.assertEquals(Integer.parseInt(data), read.size());
	}

	@When("^get the Key value of '(.*)' & '(.*)' and verify with the total '(.*)'$")
	public void validate_the_total_value(String key, String key1, String count) {
		String Actual = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
		System.out.println(Actual);
		String Actual1 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key1);
		System.out.println(Actual1);
		double sum = Double.parseDouble(Actual) + Double.parseDouble(Actual1);
		System.out.println(sum);
		String data = Double.toString(sum);
		String Actual3 = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), count);
		System.out.println(Actual3);
		double sum1 = Double.parseDouble(Actual3);
		String data1 = Double.toString(sum1);
		assertEquals(data, data1);

	}

	@Then("^Verify response matches expected text '(.*)'$")
	public void verify_response_contains_the_data(String Expected) {

		String Actual = RestAssuredSteps.getResponse().getBody().prettyPrint();
//System.out.println(Actual);
		assertEquals(" Json Path Value Check ", Expected, Actual);
//		equals(Actual.contains(Expected));

	}

	@Then("Match the response contains the key {string}")
	public void match_response_contains_the_data_expected(String Expected) throws Exception {
		String bdy = RestAssuredSteps.getResponse().getBody().asString();

		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(bdy);
		System.out.println(json.toString());
		if (json.containsKey(Expected)) {
			System.out.println("Key is present");
		} else {
			throw new Exception("Key " + Expected + " not Found in response body: " + bdy);
		}
	}

//	@Then("Verify Value {string} present in field {string}")
//	public void match_response_contains_the_data_expected(String Expected , String path) throws Exception {
////	String bdy = RestAssuredSteps.getResponse().getBody().asString();
//		System.out.println(Expected);
//		if(Expected.length()>0 ) {
//		String keyValue = RestAssuredSteps.getResponse().getBody().jsonPath().get(path).toString();
//	System.out.println("-------------------------------------------------------------------"+keyValue);
//	Assert.assertEquals("Value did not match", Expected.toString(), keyValue.toString());
//    }
//		}

	@Then("Verify Value {string} present in field as String {string}")
	public void Verify_Value_present_in_field_as_String(String Expected, String path) throws Exception {
//	String bdy = RestAssuredSteps.getResponse().getBody().asString();

		String keyValue = RestAssuredSteps.getResponse().getBody().jsonPath().get(path).toString();
		System.out.println("-------------------------------------------------------------------" + keyValue);
		Assert.assertEquals("Value did not match", Expected, keyValue);
	}

	@Then("Verify that value of the field {string} is null")
	public void verify_date_is_null(String path) throws Exception {
		if (path.length() > 0) {
			if (RestAssuredSteps.getResponse().getBody().jsonPath().get(path) == null) {
				System.out.println(path + " is null as expected");
			} else {
				throw new Exception(path + " is not null. Value: "
						+ RestAssuredSteps.getResponse().getBody().jsonPath().get(path).toString());
			}
		}
	}

	@When("^Key '(.*)' List should not contain more than '(.*)' set of records$")
	public static void List_should_contain_5_set_of_records(String key, int key1) throws Exception {
		List<String> read = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), key);
		int data = read.size();
//		String str1 = Integer.toString(data);
		System.out.println(data);
		try {
			int data1 = data;
//		boolean data11 =true;

			if (data1 <= key1) {
				boolean data11 = true;
				System.out.println("contain 5 set of records");
				assertTrue(data11);
			} else {
				assertTrue(false);
			}
		}

		catch (Exception e) {

			e.printStackTrace();
			throw new CucumberException(e);
//		System.out.println("It contains more than 5 records");
		}

	}

	@When("Print Data {string}")
	public void print_Data(String path) throws Exception {

		String data = RestAssuredSteps.getResponse().getBody().jsonPath().get(path);
		if (data != null) {
			System.out.println(path + " is not null " + data);
		} else {
			throw new Exception(path + " is null. Value: " + data);
		}
	}

	@When("^Get the response for Elastic Search Index and Pass the param$")
	public static void get_response_for_elastic_search_index_for_contact_merge() throws ParseException {
		String asString = RestAssuredSteps.getResponse().getBody().asString();
		List read = JsonPath.read(asString, "$.masterCustCode.[*]customerCode");
//		List arr = new ArrayList();
		for (int i = 0; i < read.size(); i++) {
			Object data = JsonPath.read(asString, "$.masterCustCode.[" + i + "]customerCode");
			String Data = data.toString();
			System.out.println("value of " + i + " is" + Data);

			String string2 = "CUST_UPSERT";
			spec = given().queryParam("flowType", string2);
			spec = given().queryParam("childId", Data);

		}

	}

	@When("^Get the sum of the Total Sale '(.*)' & '(.*)'$")
	public void validate_the_total_value_of_sale(String key, String key1) {
		String MCdata = HashMapContainer.get(key);
		String CCdata = HashMapContainer.get(key1);
		String MCcode = "$[0]." + MCdata + ".OrderTotalAmount";
		System.out.println(MCcode);
		String CCcode = "$[1]." + CCdata + ".OrderTotalAmount";
		System.out.println("-----------------" + CCcode);

		double ActualMCcode = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), MCcode);

		System.out.println(ActualMCcode);
		double ActualCCcode = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), CCcode);
		System.out.println(ActualCCcode);
		double sum = ActualMCcode + ActualCCcode;
		System.out.println(sum);
		String data = Double.toString(sum);
		HashMapContainer.add("$$LifeTimeValue", data);
	}


	@And("^Get the latest Date from '(.*)'$")
	public void Get_the_latest_Date_from(String path) throws ParseException {
		List<String> read = JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), path);
		System.out.println("---------------------------------" + read);

		String latest = Collections.max(read);
		log.info(latest);

		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = format.parse(latest);
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		String LastOrderDate = sdf1.format(date);
		log.info(LastOrderDate);
		HashMapContainer.add("$$LastUpdatedate", LastOrderDate);

	}
	
	
	@And("^Get the datas Upserted in Salesforce for Store$")
	public void Upserting_the_data_iteratively_Store()  throws Exception {
		// TODO Auto-generated method stub
        int i=1;
        while(i<3){
System.out.println(i);
		RestAssuredSteps steps = new RestAssuredSteps();
		MF_MuleSoft_CustomStep Csteps = new MF_MuleSoft_CustomStep();
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
		steps.header_key_value("Content-Type", "application/json");
		steps.request_body("StoreUpsert/EmailConfig");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
		steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
		steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
		
		steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("StoreUpsert/CompletedEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
		
		steps.request_body("StoreUpsert/EmailNotification");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
		Thread.sleep(30000);
		Csteps.i_verify_the_email_body_content();
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp");
	steps.content_type("application/json");
//		steps.header_key_value("Content-Type", 'application/json");
		steps.request_body("StoreUpsert/StoreTimestamp");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/insert-log-details");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		steps.request_body("StoreUpsert/InsertLogDetails");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/fetch-service-run");
		steps.content_type("application/json");
	steps.header_key_value("Content-Type", "application/json");
		steps.request_body("StoreUpsert/FetchService");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.retrivedata_store("$.serviceRunRecord[0].lastRunDateTime", "LastRunDateTime");
		steps.retrivedata_store("$.serviceRunRecord[0].timestamp", "TimeStamp");
		steps.retrivedata_store("$.serviceRunRecord[0].lastProcessedKey", "LastProcessedKey");
		
	
		steps.endPoint(
				"http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("StoreUpsert/ElasticSearchIndex", "lastRunDate", "$$LastRunDateTime");
		steps.update_String_data_request("StoreUpsert/ElasticSearchIndex", "lastProcessedKey", "$$LastProcessedKey");
		steps.update_String_data_request("StoreUpsert/ElasticSearchIndex", "timest", "$$TimeStamp");
		steps.update_String_data_request("StoreUpsert/ElasticSearchIndex", "esDeltaQuerySize", "5");
		steps.request_body("StoreUpsert/ElasticSearchIndex");
		steps.Method("Post");
		steps.print_response();
		steps.statuscode(200);
		String response1 =RestAssuredSteps.getResponse().getBody().asString();
		System.out.println("##############################"+response1.length());
		if(response1.length()<5){
			
			steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
			steps.header_key_value("Content-Type", "application/json");
			steps.request_body("StoreUpsert/StoreFailureEmailConfig");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
			steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
			steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
			
			steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
			steps.header_key_value("Content-Type", "application/json");
			steps.update_String_data_request("StoreUpsert/StoreFailureEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
			
			steps.request_body("StoreUpsert/StoreFailureEmailNotification");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
			Thread.sleep(30000);
			Csteps.mf_i_verify_the_Failure_email_body_content();
			
			break;
		}  else {
		
		
		steps.retrivedata_store("$.[4]ES_MODIFIEDDATETIME", "ModifiedTime");
		Csteps.print_Data("ES_MODIFIEDDATETIME[4]");
		steps.retrivedata_store("$.[4]Id", "ModifiedID");
		Csteps.print_Data("Id[4]");
		steps.key_count_present_in_the_response("$.[*]Id", "5");
		Csteps.get_response_for_elastic_search_index_and_store_the_keys();
		
		steps.endPoint("http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/warehouseList");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		steps.Method("Get");
		steps.print_response();
		steps.statuscode(200);
//		Csteps.getmf_id_servicengDCno();

		steps.endPoint("http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		Csteps.update_the_customer_upsert_data_to_sfdc();
		steps.Method("Post");
		steps.print_response();
		steps.statuscode(200);
		steps.match_JSONPath_contains("$..number_records_processed", "5");
		steps.match_JSONPath_contains("$..number_records_failed", "0");
		steps.retrivedata_store("$..number_records_failed", "Records");
		
		System.out.println("-----------------------------------------"+HashMapContainer.get("$$Records"));
		if (HashMapContainer.get("$$Records").equals("[0]"))
		{
			
			System.out.println("-----------------------------------------inside IF");
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("StoreUpsert/StoreTimestamp", "info[0].timestamp", "$$ModifiedTime");
		steps.update_String_data_request("StoreUpsert/StoreTimestamp", "info[0].last_processedkey", "$$ModifiedID");
		steps.request_body("StoreUpsert/StoreTimestamp");
		steps.Method("Post");
		
		
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
		steps.header_key_value("Content-Type", "application/json");
		steps.request_body("StoreUpsert/EmailConfig");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
		steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
		steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
		
		steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("StoreUpsert/CompletedEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
		
		steps.request_body("StoreUpsert/CompletedEmailNotification");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
		Thread.sleep(30000);
		Csteps.i_verify_the_Process_completed_email_body_content();
		
			i++;
			}else {
				System.out.println("-----------------------------------------Inside else");
				steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
				steps.header_key_value("Content-Type", "application/json");
				steps.request_body("StoreUpsert/StoreFailureEmailConfig");
				steps.Method("Post");
				
				steps.print_response();
				steps.statuscode(200);
				steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
				steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
				steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
				
				steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
				steps.header_key_value("Content-Type", "application/json");
				steps.update_String_data_request("StoreUpsert/StoreFailureEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
				
				steps.request_body("StoreUpsert/StoreFailureEmailNotification");
				steps.Method("Post");
				
				steps.print_response();
				steps.statuscode(200);
				steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
				Thread.sleep(30000);
				Csteps.mf_i_verify_the_Failure_email_body_content();
				break;
			}
        }
        }
	}
	
	@And("^Get the datas Upserted in Salesforce for ProductUpsert$")
	public void Upserting_the_data_iteratively_Product()  throws Exception {
		// TODO Auto-generated method stub
		 int i=1;
	        while(i<3){
	System.out.println(i);
		RestAssuredSteps steps = new RestAssuredSteps();
		MF_MuleSoft_CustomStep Csteps = new MF_MuleSoft_CustomStep();
		
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig");
		steps.header_key_value("Content-Type", "application/json");
		steps.request_body("ProductUpsert/ProductEmailConfig");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
		steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
		steps.match_response_contains_the_data_expected("ProductUpsert", "[0].APPLICATION_NAME");
		
		steps.endPoint("http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("ProductUpsert/ProductCompletedEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
		
		steps.request_body("ProductUpsert/ProductEmailNotification");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
		Thread.sleep(30000);
		Csteps.i_verify_the_email_body_content();
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/timestamp");
		steps.content_type("application/json");
//			steps.header_key_value("Content-Type", 'application/json");
			steps.request_body("ProductUpsert/ProductTimestamp");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			
			steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/insert-log-details");
			steps.content_type("application/json");
			steps.header_key_value("Content-Type", "application/json");
			steps.request_body("ProductUpsert/ProductInsertLogDetails");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			
			steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/fetch-service-run");
			steps.content_type("application/json");
			steps.header_key_value("Content-Type", "application/json");
			steps.request_body("ProductUpsert/ProductFetchRun");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			steps.retrivedata_store("$.serviceRunRecord[0].lastRunDateTime", "ProductLastRunDateTime");
			steps.retrivedata_store("$.serviceRunRecord[0].timestamp", "ProductTimeStamp");
			steps.retrivedata_store("$.serviceRunRecord[0].lastProcessedKey", "ProductlastProcessedKey");
		
		
		
		steps.endPoint(
				"http://mule-worker-internal-sys-es-qa.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("ProductUpsert/ProductElasticSearch", "lastRunDate", "$$ProductLastRunDateTime");
		steps.update_String_data_request("ProductUpsert/ProductElasticSearch", "timest", "$$ProductTimeStamp");
		steps.update_String_data_request("ProductUpsert/ProductElasticSearch", "lastProcessedKey", "$$ProductlastProcessedKey");
		steps.update_String_data_request("ProductUpsert/ProductElasticSearch", "esDeltaQuerySize", "2");
		steps.request_body("ProductUpsert/ProductElasticSearch");
		steps.Method("Post");
		steps.print_response();
		steps.statuscode(200);
		String response =RestAssuredSteps.getResponse().getBody().asString();
		
		System.out.println("##############################"+response.length());
		if(response.length()<5){
			
			steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
			steps.header_key_value("Content-Type", "application/json");
			steps.request_body("ProductUpsert/ProductEmailConfig");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
			steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
			steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
			
			steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
			steps.header_key_value("Content-Type", "application/json");
			steps.update_String_data_request("ProductUpsert/ProductFailureEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
			
			steps.request_body("ProductUpsert/ProductFailureEmailNotification");
			steps.Method("Post");
			
			steps.print_response();
			steps.statuscode(200);
			steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
			Thread.sleep(30000);
			Csteps.mf_i_verify_the_Failure_email_body_content();
			
			break;
		}  else {
		
		
			steps.retrivedata_store("$.[1]ES_MODIFIEDDATETIME", "ModifiedTime");
			Csteps.print_Data("ES_MODIFIEDDATETIME[1]");
			steps.retrivedata_store("$.[1]ITM_CD", "ModifiedID");
			Csteps.print_Data("ITM_CD[1]");
			steps.key_count_present_in_the_response("$.[*]ITM_CD", "2");
			
			
		Csteps.get_response_for_elastic_search_index_and_store_the__Product_keys();
		
		steps.endPoint("http://mule-worker-internal-sys-salesforce-qa-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert");
		steps.header_key_value("Content-Type", "application/json");
		Csteps.update_the_upsert_data_to_product_sfdc();
		steps.Method("Post");
		steps.print_response();
		steps.statuscode(200);
		steps.match_JSONPath_contains("$..number_records_processed", "2");
		
		steps.match_JSONPath_contains("$..number_records_failed", "0");
		steps.retrivedata_store("$..number_records_failed", "Records");
		
		System.out.println("-----------------------------------------"+HashMapContainer.get("$$Records"));
		if (HashMapContainer.get("$$Records").equals("[0]")) {
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/timestamp");
		steps.content_type("application/json");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("ProductUpsert/ProductTimestamp", "info[0].timestamp", "$$ModifiedTime");
		steps.update_String_data_request("ProductUpsert/ProductTimestamp", "info[0].last_processedkey", "$$ModifiedID");
		steps.request_body("ProductUpsert/ProductTimestamp");
		steps.Method("Post");
		steps.statuscode(200);
		
		
		steps.endPoint("http://mule-worker-internal-sys-sqlserver-uat-v1.us-w2.cloudhub.io:8091/database/emailconfig");
		steps.header_key_value("Content-Type", "application/json");
		steps.request_body("ProductUpsert/ProductEmailConfig");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
		steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
		steps.match_response_contains_the_data_expected("ProductUpsert", "[0].APPLICATION_NAME");
		
		steps.endPoint("http://mule-worker-internal-sys-email-uat-v1.us-w2.cloudhub.io:8091/mail/send-email-notification");
		steps.header_key_value("Content-Type", "application/json");
		steps.update_String_data_request("ProductUpsert/ProductCompletedEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
		
		steps.request_body("ProductUpsert/ProductCompletedEmailNotification");
		steps.Method("Post");
		
		steps.print_response();
		steps.statuscode(200);
		steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
		Thread.sleep(30000);
		Csteps.i_verify_the_Process_completed_email_body_content();
		
			i++;
			}else {
				steps.endPoint("http://mule-worker-internal-sys-sqlserver.us-w2.cloudhub.io:8091/database/emailconfig");
				steps.header_key_value("Content-Type", "application/json");
				steps.request_body("ProductUpsert/ProductEmailConfig");
				steps.Method("Post");
				
				steps.print_response();
				steps.statuscode(200);
				steps.retrivedata_store("$.[0]FROM_ADDRESS", "FROM_ADDRESS");
				steps.match_response_contains_the_data_expected("CustomerPortal", "[0].INTEGRATION_NAME");
				steps.match_response_contains_the_data_expected("StoreUpsert", "[0].APPLICATION_NAME");
				
				steps.endPoint("http://mule-worker-internal-sys-email-qa.us-w2.cloudhub.io:8091/mail/send-email-notification");
				steps.header_key_value("Content-Type", "application/json");
				steps.update_String_data_request("ProductUpsert/ProductFailureEmailNotification", "FROM_ADDRESS", "$$FROM_ADDRESS");
				
				steps.request_body("ProductUpsert/ProductFailureEmailNotification");
				steps.Method("Post");
				
				steps.print_response();
				steps.statuscode(200);
				steps.match_response_contains_the_data_expected("Email sent successfully to logeshkumar.r@royalcyber.com", "message");
				Thread.sleep(30000);
				Csteps.mf_i_verify_the_Failure_email_body_content();
				break;
			}
	}
		}
	}
	
	
	
	
	
	@When("^get ES response$")
    public void ES_response() {
       
    String str = ElasticSearchJestUtility.getJestResponseAsString();
    System.out.println("%%%%%%%%%%%"+str);
    Gson gson=new Gson();
    String jsonString1 = gson.toJson(str);
    System.out.println("JSON------------"+jsonString1);
    }
	
	
static List<TealiumFieldMapping> tealiumCSVData = new ArrayList<TealiumFieldMapping>();
static List<TealiumFieldMapping> tealiumCSVDataforDB = new ArrayList<TealiumFieldMapping>();   
    @And ("^create arraylist of customercodes object$")
    public void create_arraylist_of_customercodes_object() {
    	
    	String mastercodes = HashMapContainer.get("$$TLMMasterCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
    	String[] mastercodesArray = null;
    	mastercodesArray = mastercodes.split(",");
    	
    	System.out.println(mastercodesArray.toString());
		String customercodes = HashMapContainer.get("$$TLMCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
		
		String masterandcustomercodes = mastercodes+","+customercodes;
		String[] MandCcodesArray = null;
		MandCcodesArray = masterandcustomercodes.split(",");
		String allmasterandcustcodes = Arrays.toString(MandCcodesArray).replaceAll("\\s+", "");
		System.out.println("+masterandcustomercodes: "+allmasterandcustcodes);
		HashMapContainer.add("$$Allmasterandcustcodes", allmasterandcustcodes);
    	
    		
    		ArrayList<String> read1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), "$.aggregations.group_by_cust_cd.buckets[*]");
  
    		int arraysize = read1.size();
    		
    		for (int e=0; e<arraysize; e++) {
    			
    			String ChildCustomerCodepath = "$.aggregations.group_by_cust_cd.buckets["+e+"].view.hits.hits[*]";
    			ArrayList<String> ChildCustomerCodeArray = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), ChildCustomerCodepath);
    			int ChildCustomerCodeArraySize = ChildCustomerCodeArray.size();
    			
    			for (int j=0; j<ChildCustomerCodeArraySize; j++) {
    			
    			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
    			
    			String ultimateMasterCustomerCodepath = "$.aggregations.group_by_cust_cd.buckets["+e+"].view.hits.hits["+j+"]._source.ultimateMasterCustomerCode";
    			String ultimateMasterCustomerCodeValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), ultimateMasterCustomerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
    			tealiumFM.setMasterCustomerCode(ultimateMasterCustomerCodeValue);

    			String customerCodepath = "$.aggregations.group_by_cust_cd.buckets["+e+"].view.hits.hits["+j+"]._source.customerCode";
        		String customerCodeValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
        		tealiumFM.setCustomerCode(customerCodeValue);
        		System.out.println("read2"+ultimateMasterCustomerCodeValue);
        		System.out.println("read3"+customerCodeValue);
        			
        		tealiumCSVData.add(tealiumFM);
    			}
    		}
    		
    		for (int e=0; e<arraysize; e++) {
    			
    			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
    			
    			String MasterCustomerCodepath = "$.aggregations.group_by_cust_cd.buckets["+e+"].key";
    			String MasterCustomerCodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), MasterCustomerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
    			tealiumFM.setCustomerCode(MasterCustomerCodevalue);
    			
    			tealiumCSVData.add(tealiumFM);
    		}
    		
    		for(TealiumFieldMapping x: tealiumCSVData) {
    			System.out.println("list data in CSV:"+x.toString());
    		}
    }
    
    @And ("^create arraylist of contactId, masterContactId and type object$")
    public void create_arraylist_of_contactId_masterContactId_and_type_object() {
    	
    	System.out.println("create_arraylist_of_contactId_masterContactId_and_type_object");
    	
    	String response = ElasticSearchJestUtility.getJestResponseAsString().toString();
    	
    	System.out.println("ES Response: "+response);
    	
    	ArrayList<String> read1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), "$.aggregations.group_by_cust_cd.buckets[*]");
    	  
		int arraysize = read1.size();
		System.out.println("array sizee"+arraysize);
		for (int i=0; i<arraysize; i++) {
			
			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
			
			String customercodepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].key";
			String customercodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customercodepath).toString();
			
			String contactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.contactId";
			String contactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), contactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String typepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.type";
			String typevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), typepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String masterContactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.masterContactId";
			String masterContactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), masterContactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			for(TealiumFieldMapping x: tealiumCSVData) {
				
				System.out.println("First customercodevalue in the bucket:"+customercodevalue);
				 
				System.out.println("Customer Code in Object"+x.getCustomerCode());
				if(x.getCustomerCode().contains(customercodevalue)) {
					int listItemIndex = tealiumCSVData.indexOf(x);
					System.out.println("Inside IF condition");
					x.setSalesforceContactId(contactId);
					x.setSalesforceMasterContactId(masterContactId);
					x.setRecordType(typevalue);
					tealiumCSVData.set(listItemIndex, x);
				}else {
					//else statement;
				}
			}
		
		}
		for(TealiumFieldMapping x: tealiumCSVData) {
			
			System.out.println("list data in CSV:"+x.toString());
		}
    }
    
    @And("^create arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object only DB$")
    public void create_arraylist_of_ZIP_CD_EMAIL_ADDR_HOME_PHONE_and_BUS_PHONE_object_only_DB() {
    	
    	ArrayList<String> read1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), "$.aggregations.cust_cd.buckets[*]");
  	  
		int arraysize = read1.size();
		System.out.println("array size"+arraysize);
		for (int i=0; i<arraysize; i++) {
			
			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
			
			String customercodepath = "$.aggregations.cust_cd.buckets["+i+"].key";
			String customercodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customercodepath).toString();
			
			String ZIP_CDpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.ZIP_CD";
			String ZIP_CDValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), ZIP_CDpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String EMAIL_ADDRpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.EMAIL_ADDR";
			String EMAIL_ADDRvalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), EMAIL_ADDRpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String HOME_PHONEpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.HOME_PHONE";
			String HOME_PHONEValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), HOME_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String BUS_PHONEpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.BUS_PHONE";
			String BUS_PHONEValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), BUS_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			for(TealiumFieldMapping x: tealiumCSVData) {
				
				System.out.println("First customercodevalue in the bucket:"+customercodevalue);
				 
				System.out.println("Customer Code in Object"+x.getCustomerCode());
				if(x.getCustomerCode().contains(customercodevalue)) {
					int listItemIndex = tealiumCSVData.indexOf(x);
					System.out.println("Inside IF condition");
					x.setZipCode(ZIP_CDValue);
					x.setEmailAddress(EMAIL_ADDRvalue);
					x.setHomePhone(HOME_PHONEValue);
					x.setBusPhone(BUS_PHONEValue);
					tealiumCSVData.set(listItemIndex, x);
				}else {
					//else statement;
				}
			}
		}
		for(TealiumFieldMapping x: tealiumCSVData) {
			
			System.out.println("list data in CSV:"+x.toString());
		}
    }
	
    
    
    @And("^create arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object$")
    public void create_arraylist_of_ZIP_CD_EMAIL_ADDR_HOME_PHONE_and_BUS_PHONE_object() {
    	
    	ArrayList<String> read1 = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), "$.aggregations.cust_cd.buckets[*]");
  	  
		int arraysize = read1.size();
		System.out.println("array size"+arraysize);
		for (int i=0; i<arraysize; i++) {
			
			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
			
			String customercodepath = "$.aggregations.cust_cd.buckets["+i+"].key";
			String customercodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customercodepath).toString();
			
			String ZIP_CDpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.ZIP_CD";
			String ZIP_CDValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), ZIP_CDpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String EMAIL_ADDRpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.EMAIL_ADDR";
			String EMAIL_ADDRvalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), EMAIL_ADDRpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String HOME_PHONEpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.HOME_PHONE";
			String HOME_PHONEValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), HOME_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String BUS_PHONEpath = "$.aggregations.cust_cd.buckets["+i+"].view.hits.hits[*]._source.BUS_PHONE";
			String BUS_PHONEValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), BUS_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			for(TealiumFieldMapping x: tealiumCSVDataforDB) {
				
				System.out.println("First customercodevalue in the bucket:"+customercodevalue);
				 
				System.out.println("Customer Code in Object"+x.getCustomerCode());
				if(x.getCustomerCode().contains(customercodevalue)) {
					int listItemIndex = tealiumCSVDataforDB.indexOf(x);
					System.out.println("Inside IF condition");
					x.setZipCode(ZIP_CDValue);
					x.setEmailAddress(EMAIL_ADDRvalue);
					x.setHomePhone(HOME_PHONEValue);
					x.setBusPhone(BUS_PHONEValue);
					tealiumCSVDataforDB.set(listItemIndex, x);
				}else {
					//else statement;
				}
			}
		}
		for(TealiumFieldMapping x: tealiumCSVDataforDB) {
			
			System.out.println("list data in CSV:"+x.toString());
		}
    }
	
    
    
    @And ("^create CSV file '(.*)' using all the datas that are collected$")
    public void create_CSV_file_using_all_the_datas_collected(String filepath) throws IOException {
        
        File file = new File(filepath);
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("salesforceContactId,zipCode,recordType,homePhone,customerCode,busPhone,emailAddress,salesforceMasterContactId,masterCustomerCode,DataIngestionType");
        bw.newLine();

 

        for (TealiumFieldMapping x : tealiumCSVData) {
            bw.write(x.getSalesforceContactId()+","+x.getZipCode()+","+x.getRecordType()+","+x.getHomePhone()+","+x.getCustomerCode()+","+x.getBusPhone()+","+x.getEmailAddress()+","+x.getSalesforceMasterContactId()+","+x.getMasterCustomerCode()+","+"ES Dedupe Cust");
            bw.newLine();
        }
        bw.close();
        fw.close();
    }
	
    @And ("^add master codes and customer codes to pass for further queries$")
    public void add_mastercodes_and_customer_codes_to_pass_for_further_queries() {
       
        String mastercodes = HashMapContainer.get("$$TLMMasterCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String customercodes = HashMapContainer.get("$$TLMCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
       
        String masterandcustomercodes = mastercodes+","+customercodes;
       
        String[] MandCcodesArray = null;
        MandCcodesArray = masterandcustomercodes.split(",");
        String allmasterandcustcodes = Arrays.toString(MandCcodesArray).replaceAll("\\s+", "");
        System.out.println("+masterandcustomercodes: "+allmasterandcustcodes);
        HashMapContainer.add("$$Allmasterandcustcodes", allmasterandcustcodes);
    }
	
    
    @And ("^Add master codes and customer codes to pass for further queries in DB Validations$")
    public void add_mastercodes_and_customer_codes_to_pass_for_further_queries_In_DB_Validations() {
       
        String mastercodes = HashMapContainer.get("$$APIResponse").replace("[", "").replace("]", "").replaceAll("\\s+", "");
//        int Size=mastercodes.length();
//        String Size1=String.valueOf(Size).replace("[", "").replace("]", "").replaceAll("\\s+", "");
//        HashMapContainer.add("$$Size", Size1);
        String customercodes = HashMapContainer.get("$$TLMCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
       
        String masterandcustomercodes = mastercodes+","+customercodes;
       
        String[] MandCcodesArray = null;
        MandCcodesArray = masterandcustomercodes.split(",");
        String allmasterandcustcodes = Arrays.toString(MandCcodesArray).replaceAll("\\s+", "");
        System.out.println("+masterandcustomercodes: "+allmasterandcustcodes);
        HashMapContainer.add("$$Allmasterandcustcodes", allmasterandcustcodes);
    }
    
    
    
    @And ("^Add master codes and customer codes to pass for further queries in DB Validations with DB inputs$")
    public void add_mastercodes_and_customer_codes_to_pass_for_further_queries_In_DB_Validations_with_DB_inputs() {
       
        String mastercodes = HashMapContainer.get("$$DBMasterCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String customercodes = HashMapContainer.get("$$DBCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
       
        String masterandcustomercodes = mastercodes+","+customercodes;
       
        String[] MandCcodesArray = null;
        MandCcodesArray = masterandcustomercodes.split(",");
        String allmasterandcustcodes = Arrays.toString(MandCcodesArray).replaceAll("\\s+", "");
        System.out.println("+masterandcustomercodes: +++++++++++++++++++++++++++++++++++++"+allmasterandcustcodes);
        HashMapContainer.add("$$DBAllmasterandcustcodes", allmasterandcustcodes);
    }
    
    @And ("^add master codes and customer codes to pass for further queries for DB$")
    public void add_mastercodes_and_customer_codes_to_pass_for_further_queries_for_DB() {
       
        String mastercodes = HashMapContainer.get("$$TLMDBMasterCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String customercodes = HashMapContainer.get("$$TLMDBCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
       
        String masterandcustomercodes = mastercodes+","+customercodes;
       
        String[] MandCcodesArray = null;
        MandCcodesArray = masterandcustomercodes.split(",");
        String allmasterandcustcodes = Arrays.toString(MandCcodesArray).replaceAll("\\s+", "");
        System.out.println("+masterandcustomercodes: "+allmasterandcustcodes);
        HashMapContainer.add("$$AllDBmasterandcustcodes", allmasterandcustcodes);
    }
    
    
    @And ("^create arraylist of master codes, customer codes, contactId, masterContactId and type object only DB$")
    public void create_arraylist_of_master_and_customer_codes_contactId_masterContactId_and_type_object_Only_DB() {
       
        String MandCcodes = HashMapContainer.get("$$Allmasterandcustcodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] MandCcodesArray = null;
        MandCcodesArray = MandCcodes.split(",");
        int MandCcodesArraySize = MandCcodesArray.length;
       
       
        String sfKeys = HashMapContainer.get("$$TLMResponseCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] sfKeysArray = null;
        sfKeysArray = sfKeys.split(",");
        int sfKeysArraySize = sfKeysArray.length;
       
           
            for (int w=0; w<MandCcodesArraySize; w++){
               
                for (int i=0; i<sfKeysArraySize; i++) {
                   
                    if(MandCcodesArray[w].equals(sfKeysArray[i])) {
                       
                        TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
                       
                        String customercodepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].key";
                        String customercodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customercodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String contactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.contactId";
                        String contactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), contactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String typepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.type";
                        String typevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), typepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String masterContactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.masterContactId";
                        String masterContactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), masterContactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String masterCustomerCodepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.masterCustomerCode";
                        String masterCustomerCodeValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), masterCustomerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                                tealiumFM.setCustomerCode(customercodevalue);
                                tealiumFM.setSalesforceContactId(contactId);
                                tealiumFM.setRecordType(typevalue);
                                if (typevalue.contains("MASTER")) {
                                    tealiumFM.setMasterCustomerCode(customercodevalue);
                                    tealiumFM.setSalesforceMasterContactId(contactId);
                                }else {
                                    tealiumFM.setMasterCustomerCode(masterCustomerCodeValue);
                                    tealiumFM.setSalesforceMasterContactId(masterContactId);
                                }
                               
                                tealiumCSVData.add(tealiumFM);
                    }else {
                    }
                }
               
        }
            for(TealiumFieldMapping x: tealiumCSVData) {
               
                System.out.println("list data in CSV:"+x.toString());
            }
    }
    
    
    @And ("^create arraylist of master codes, customer codes, contactId, masterContactId and type object$")
    public void create_arraylist_of_master_and_customer_codes_contactId_masterContactId_and_type_object() {
       
        String MandCcodes = HashMapContainer.get("$$AllDBmasterandcustcodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] MandCcodesArray = null;
        MandCcodesArray = MandCcodes.split(",");
        int MandCcodesArraySize = MandCcodesArray.length;
       
       
        String sfKeys = HashMapContainer.get("$$TLMDBResponseCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] sfKeysArray = null;
        sfKeysArray = sfKeys.split(",");
        int sfKeysArraySize = sfKeysArray.length;
       
           
            for (int w=0; w<MandCcodesArraySize; w++){
               
                for (int i=0; i<sfKeysArraySize; i++) {
                   
                    if(MandCcodesArray[w].equals(sfKeysArray[i])) {
                       
                        TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
                       
                        String customercodepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].key";
                        String customercodevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), customercodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String contactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.contactId";
                        String contactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), contactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String typepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.type";
                        String typevalue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), typepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String masterContactIdpath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.masterContactId";
                        String masterContactId = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), masterContactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                        String masterCustomerCodepath = "$.aggregations.group_by_cust_cd.buckets["+i+"].view.hits.hits[*]._source.masterCustomerCode";
                        String masterCustomerCodeValue = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.getJestResponseAsString(), masterCustomerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
                       
                                tealiumFM.setCustomerCode(customercodevalue);
                                tealiumFM.setSalesforceContactId(contactId);
                                tealiumFM.setRecordType(typevalue);
                                if (typevalue.contains("MASTER")) {
                                    tealiumFM.setMasterCustomerCode(customercodevalue);
                                    tealiumFM.setSalesforceMasterContactId(contactId);
                                }else {
                                    tealiumFM.setMasterCustomerCode(masterCustomerCodeValue);
                                    tealiumFM.setSalesforceMasterContactId(masterContactId);
                                }
                               
                                tealiumCSVDataforDB.add(tealiumFM);
                    }else {
                    }
                }
               
        }
            for(TealiumFieldMapping x: tealiumCSVDataforDB) {
               
                System.out.println("list data in CSV:"+x.toString());
            }
    }
    
    
    @And ("^create arraylist of master codes$")
    public void create_arraylist_of_master() {
     
    String mastercodes = HashMapContainer.get("$$Customercodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
    System.out.println("########################################"+mastercodes);
//    HashMapContainer.add("$$Mcodes", mastercodes);
    String[] mastercodesArray = null;
	mastercodesArray = mastercodes.split(",");
	String allmasterandcustcodes = Arrays.toString(mastercodesArray).replace("[", "").replace("]", "").replaceAll("\\s+", "");
	System.out.println("########################################"+allmasterandcustcodes);
//	String Mcodes =mastercodesArray.toString();
	HashMapContainer.add("$$Mcodes", allmasterandcustcodes);
    }
    
    @And ("^create arraylist of master codes and CustCodes$")
    public void create_arraylist_of_master_cust() throws Exception {
    	RestAssuredSteps rs = new RestAssuredSteps();
    	String CustomerCodes = RestAssuredSteps.getResponse().getBody().jsonPath().get("$.[*]").toString();
    	System.out.println(CustomerCodes);
    	CustomerCodes = CustomerCodes.replace("[", "").replace("]", "").replaceAll("\\s+", "");
    	String MasterCodes= HashMapContainer.get("$$Mcodes");
    	
    	String MasterCustCodes= CustomerCodes+MasterCodes;
    	
    	rs.update_array_data_request("TealiumCustMasterCodes", "$.customerCodes", MasterCustCodes);   	
    }
    
    
    @And("^create API response arraylist of master codes, customer codes, contactId, masterContactId and type object$")
    public void create_API_response_arraylist_of_master_and_customer_codes_contactId_masterContactId_and_type_object() {
    	
		String MandCcodes = HashMapContainer.get("$$Allmasterandcustcodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
    	String[] MandCcodesArray = null;
    	MandCcodesArray = MandCcodes.split(",");
    	int MandCcodesArraySize = MandCcodesArray.length;
    	
    	String responseCustomerCodes = HashMapContainer.get("$$TLMResponseCustomerCodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
    	String[] responseCustomerCodesArray = null;
    	responseCustomerCodesArray = responseCustomerCodes.split(",");
    	int responseCustomerCodesArraySize = responseCustomerCodesArray.length;
		
			for (int w=0; w<MandCcodesArraySize; w++){
				
				for (int i=0; i<responseCustomerCodesArraySize; i++) {
					
					if(MandCcodesArray[w].equals(responseCustomerCodesArray[i])) {
						
						TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
						
						String customercodepath = "$.["+i+"]..customerCode";
						String customercodevalue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), customercodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
						
						String contactIdpath = "$.["+i+"]..contactId";
						String contactId = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), contactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
						
						String typepath = "$.["+i+"]..type";
						String typevalue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), typepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
						
						String masterContactIdpath = "$.["+i+"]..masterContactId";
						String masterContactId = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), masterContactIdpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
						
						String masterCustomerCodepath = "$.["+i+"]..masterCustomerCode";
						String masterCustomerCodeValue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), masterCustomerCodepath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
						
								tealiumFM.setCustomerCode(customercodevalue);
								tealiumFM.setSalesforceContactId(contactId);
								tealiumFM.setRecordType(typevalue);
//								String type = "MASTER";
								if (typevalue.contains("MASTER")) {
									tealiumFM.setMasterCustomerCode(customercodevalue);
									tealiumFM.setSalesforceMasterContactId(contactId);
								}else {
									tealiumFM.setMasterCustomerCode(masterCustomerCodeValue);
									tealiumFM.setSalesforceMasterContactId(masterContactId);
								}
								
								tealiumCSVData.add(tealiumFM);
								break;
					}
					else {
						
					}
					
				}
		}
			for(TealiumFieldMapping x: tealiumCSVData) {
				
				System.out.println("list data in CSV:"+x.toString());
			}
    }
    
    @And("^create API response arraylist of ZIP_CD, EMAIL_ADDR, HOME_PHONE and BUS_PHONE object$")
    public void create_API_response_arraylist_of_ZIP_CD_EMAIL_ADDR_HOME_PHONE_and_BUS_PHONE_object() {
    	
		String MandCcodes = HashMapContainer.get("$$Allmasterandcustcodes").replace("[", "").replace("]", "").replaceAll("\\s+", "");
    	String[] MandCcodesArray = null;
    	MandCcodesArray = MandCcodes.split(",");
    	int MandCcodesArraySize = MandCcodesArray.length;
    	
		for (int i=0; i<MandCcodesArraySize; i++) {

			TealiumFieldMapping tealiumFM = new TealiumFieldMapping();
			
			String MandCArrayValue = MandCcodesArray[i].replaceAll("^\"|\"$", "");
			String path = "$.."+MandCArrayValue;

			String value = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), path).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");

			if(value.contains("EMAIL_ADDR")) {

			String customercodevalue=MandCArrayValue;
			String ZIP_CDpath = "$.."+MandCArrayValue+".ZIP_CD";
			String ZIP_CDValue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), ZIP_CDpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String EMAIL_ADDRpath = "$.."+MandCArrayValue+".EMAIL_ADDR";
			String EMAIL_ADDRvalue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), EMAIL_ADDRpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");

			String HOME_PHONEpath = "$.."+MandCArrayValue+".HOME_PHONE";
			String HOME_PHONEValue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), HOME_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");
			
			String BUS_PHONEpath = "$.."+MandCArrayValue+".BUS_PHONE";
			String BUS_PHONEValue = com.jayway.jsonpath.JsonPath.read(RestAssuredSteps.getResponse().getBody().asString(), BUS_PHONEpath).toString().replace("[", "").replace("]", "").replaceAll("\\s+", "");

			for(TealiumFieldMapping x: tealiumCSVData) {
				
				System.out.println("Customer Code in Object"+x.getCustomerCode());
				if(x.getCustomerCode().contains(customercodevalue)) {
					int listItemIndex = tealiumCSVData.indexOf(x);
					System.out.println("Inside IF condition");
					x.setZipCode(ZIP_CDValue);
					x.setEmailAddress(EMAIL_ADDRvalue);
					x.setHomePhone(HOME_PHONEValue);
					x.setBusPhone(BUS_PHONEValue);
					tealiumCSVData.set(listItemIndex, x);
				}else {
					//else statement;
				}
			}
			}else {
				
			}
		}
		for(TealiumFieldMapping x: tealiumCSVData) {
			
			System.out.println("list data in CSV:"+x.toString());
		}
    } 

    
    @And("^Compare API response with DB query Response$")
    public void compare_response_master_codes_between_API_and_DB_query() {
       
    	
        String APIResponse = HashMapContainer.get("$$APIResponse").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] APIResponseArray = null;
        APIResponseArray = APIResponse.split(",");
//        String arr=Arrays.toString(MandCcodesArray);
       int APISize = APIResponseArray.length;
       System.out.println("__________________________API SIZE "+APISize);
        String DBQueryResponse = HashMapContainer.get("$$DBQueryResponse").replace("[", "").replace("]", "").replaceAll("\\s+", "");
        String[] DBQueryResponseArray = null;
        DBQueryResponseArray = DBQueryResponse.split(",");
        int DBSize = DBQueryResponseArray.length;
        System.out.println("__________________________DB SIZE "+DBSize);
        assertEquals(APISize, DBSize);
        
        List<String> API = Arrays.asList(APIResponseArray);
        System.out.println("+++++++++++++++"+ API);
        Collections.sort(API);
        System.out.println("*************"+API);
        List<String> DBQuery = Arrays.asList(DBQueryResponseArray);
        System.out.println("+++++++++++++++"+ DBQuery);
        Collections.sort(DBQuery);
        System.out.println("*************"+DBQuery);
//        ArrayList<String> list3=new ArrayList<String>();    
        
        assertEquals(API, DBQuery);}
//        assertThat(API, containsInAnyOrder(DBQuery));
    
//        for(int i=0;i<DBSize;i++)
//        {
//            for(int j=0;j<APISize;j++)
//            {                       
////                if(DBQuery.contains(API.get(j))==true)
////                {
//                	System.out.println("if condition1 passed");
//                    if(DBQuery.get(i).equals(API.get(j)))
//                    {
//                    	System.out.println("if condition2 passed");
//                        list3.add(DBQuery.get(i));
//                        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\"+list3);
//                    }   
////                }
////                else{ 
////                }
//            }
//        }
//System.out.println("*************************************************"+list3);
//        assertEquals(API, list3);
////        if(API.equals(list3))
////        {
////            System.out.println("true");
////        }
////        else{System.out.println("false");
////        }
//    }

        
        
//        
//        for(int i=0;i<Size;i++)
//        {
//        	for(int j=0;j<UlimateSize;j++)	
//        	{
//        		
////        		List<String> codes=new ArrayList<String>();
////        		List<String> codes1=new ArrayList<String>();
//        		if(MandCcodesArray[i].equals(TLMUltimateMasterCodesArray[j])){
//        			System.out.println(MandCcodesArray[i]+" is equal to "+TLMUltimateMasterCodesArray[j]);
//        		
////        		codes.add(MandCcodesArray[i]);
////        		codes1.add(TLMUltimateMasterCodesArray[j]);
//        			
//        		}	else {
//        			
//        			System.out.println("API response "+MandCcodesArray[i]+" is not equal to DB response "+TLMUltimateMasterCodesArray[j]+" So Validating the next Record");
//        			
//        		}
////        		assertEquals(codes, codes1); 
//        		
//                	}        }  
        
        


//        assertEquals(MandCcodesArray, TLMUltimateMasterCodesArray);
        
    @And("^Validate API generated CSV file data with DB generated CSV file data$")
    public void Validate_API_generated_CSV_file_data_with_DB_generated_CSV_file_data() {
        int recordIteration = 0;
        int totalIterations = tealiumCSVData.size()*tealiumCSVDataforDB.size();
       
        if (tealiumCSVData.size()==tealiumCSVDataforDB.size()) {
           
            for (TealiumFieldMapping x: tealiumCSVData) {
               
                for (TealiumFieldMapping y: tealiumCSVDataforDB) {
                   
                    if (x.getCustomerCode().contains(y.getCustomerCode())) {
                       
                        assertEquals(x.getBusPhone(), y.getBusPhone());
                        assertEquals(x.getEmailAddress(), y.getEmailAddress());
                        assertEquals(x.getHomePhone(), y.getHomePhone());
                        assertEquals(x.getMasterCustomerCode().replaceAll("^\"|\"$", ""), y.getMasterCustomerCode().replaceAll("^\"|\"$", ""));
                        assertEquals(x.getRecordType(), y.getRecordType());
                        assertEquals(x.getSalesforceContactId(), y.getSalesforceContactId());
                        assertEquals(x.getSalesforceMasterContactId(), y.getSalesforceMasterContactId());
                        assertEquals(x.getZipCode(), y.getZipCode());
                        break;
                    }else {
                        System.out.println(x.getCustomerCode()+" doesn't match with "+y.getCustomerCode()+".So, checking with next record");
                        recordIteration++;
                    }
                }
            }
       
            if (recordIteration==totalIterations) {
                System.out.println("recordIteration: "+recordIteration);
                System.out.println("totalIterations: "+totalIterations);
                assertEquals("Array Records should match", "None of the Array records are matching");
            }
           
        }else {
            System.out.println("Assert fails due to Row count mismatch between API row count: "+tealiumCSVData.size()+" and DB row count:"+tealiumCSVDataforDB.size());
            assertEquals(tealiumCSVData.size(), tealiumCSVDataforDB.size());
        }
    }
    
    @And ("^create CSV file '(.*)' using all the datas that are collected from DB queries$")
    public void create_CSV_file_using_all_the_datas_collected_from_DB_queries(String filepath) throws IOException {
        
        File file = new File(filepath);
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("salesforceContactId,zipCode,recordType,homePhone,customerCode,busPhone,emailAddress,salesforceMasterContactId,masterCustomerCode,DataIngestionType");
        bw.newLine();

 

        for (TealiumFieldMapping x : tealiumCSVDataforDB) {
            
            bw.write(x.getSalesforceContactId()+","+x.getZipCode()+","+x.getRecordType()+","+x.getHomePhone()+","+x.getCustomerCode()+","+x.getBusPhone()+","+x.getEmailAddress()+","+x.getSalesforceMasterContactId()+","+x.getMasterCustomerCode()+","+"ES Dedupe Cust");
            bw.newLine();
        }
        bw.close();
        fw.close();
    }
	public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}
