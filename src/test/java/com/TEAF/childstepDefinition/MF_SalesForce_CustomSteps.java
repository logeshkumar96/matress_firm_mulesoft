package com.TEAF.childstepDefinition;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.TEAF.stepDefinitions.CommonSteps;

import cucumber.runtime.CucumberException;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * @ScriptName : Utilities
 * @Description : This class contains Commonly used Keyword for Mobile/Web
 *              application automation using Cucumber framework
 * @Author : Swathin Ratheendren	
 * @Creation Date : September 2016 @Modified Date:
 */
public class MF_SalesForce_CustomSteps{
	
	static Logger log = Logger.getLogger(MF_SalesForce_CustomSteps.class.getName());

	private static WebDriver driver;
	
	@Given("^I login to Mattress Firm Sales Force with valid '(.*)' User Credentials$")
	public static void I_Login_to_MF_SF_ValidCreds(String userType) throws Exception {
		try {			
			CommonSteps.I_enter_in_field("127504271@mfrm.com.uat", "login_username");
			CommonSteps.I_enter_in_field("RCyber@2020!", "login_password");
			CommonSteps.I_click("login_submitBtn");
			CommonSteps.I_pause_for_seconds(4);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Then("^I hit tab-key on element '(.*)'$")
    public static void I_hit_tab_key_on_element(String element) {
        try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element), 90000);
            wElement.sendKeys(Keys.TAB);
        } catch (Exception e) {
            log.error(e);
            throw new CucumberException(e.getMessage(), e);
        }
    }
	
	@Given("^I switch to Contact '(.*)' tab$")
	public static void I_switch_to_contact_Tab(String contactName) throws Exception {
		try {
			driver = StepBase.getDriver();
			WebElement contactTabelement= driver.findElement(By.xpath("//span[contains(text(),'"+contactName+"')]"));
			contactTabelement.click();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Given("^I verify if Contact is Portal User '(.*)'$")
	public static void I_verify_if_SF_PortalUser(String isPortalUser) throws Exception {
		try {
			driver = StepBase.getDriver();
			if(isPortalUser.equalsIgnoreCase("yes")) {
			WebElement isPortalUserFlag = driver.findElement(GetPageObjectRead.OR_GetElement("PortalUserFlag"));
			WrapperFunctions.highLightElement(isPortalUserFlag);
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	@Given("^I close '(.*)' contact tab$")
	public static void I_close_MF_SF_ContactTab(String contactName) throws Exception {
		try {
			driver = StepBase.getDriver();
			WebElement closeContact = driver.findElement(By.xpath("//span[contains(text(),'Close "+contactName+"')]"));
			WrapperFunctions.clickByJS(closeContact);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	
	
}
