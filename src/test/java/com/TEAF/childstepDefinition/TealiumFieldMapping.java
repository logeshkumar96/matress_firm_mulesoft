package com.TEAF.childstepDefinition;



public class TealiumFieldMapping {
	
	@Override
	public String toString() {
		return "TealiumFieldMapping [salesforceContactId=" + salesforceContactId + ", zipCode=" + zipCode
				+ ", recordType=" + recordType + ", homePhone=" + homePhone + ", customerCode=" + customerCode
				+ ", busPhone=" + busPhone + ", emailAddress=" + emailAddress + ", salesforceMasterContactId="
				+ salesforceMasterContactId + ", masterCustomerCode=" + masterCustomerCode + "]";
	}
	String salesforceContactId;
	String zipCode;
	String recordType;
	String homePhone;
	String customerCode;
	String busPhone;
	String emailAddress;
	String salesforceMasterContactId;
	String masterCustomerCode;
	
	public String getSalesforceContactId() {
		return salesforceContactId;
	}
	public void setSalesforceContactId(String salesforceContactId) {
		this.salesforceContactId = salesforceContactId;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zIP_CDValue) {
		this.zipCode = zIP_CDValue;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getBusPhone() {
		return busPhone;
	}
	public void setBusPhone(String busPhone) {
		this.busPhone = busPhone;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getSalesforceMasterContactId() {
		return salesforceMasterContactId;
	}
	public void setSalesforceMasterContactId(String salesforceMasterContactId) {
		this.salesforceMasterContactId = salesforceMasterContactId;
	}
	public String getMasterCustomerCode() {
		return masterCustomerCode;
	}
	
	public void setMasterCustomerCode(String masterCustomerCode) {
		this.masterCustomerCode = masterCustomerCode;
	}

}
