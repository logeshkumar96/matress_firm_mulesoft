package com.TEAF.childstepDefinition;

public class SfdcRequestPayload 
{
    private String Customer_Code__c;

    private String Last_Order_Date__c;

    private int Number_of_Orders__c;

    private double LifeTime_Value__c;

    public void setCustomer_Code__c(String Customer_Code__c){
        this.Customer_Code__c = Customer_Code__c;
    }
    public String getCustomer_Code__c(){
        return this.Customer_Code__c;
    }
    public void setLast_Order_Date__c(String Last_Order_Date__c){
        this.Last_Order_Date__c = Last_Order_Date__c;
    }
    public String getLast_Order_Date__c(){
        return this.Last_Order_Date__c;
    }
    public void setNumber_of_Orders__c(int totalOrdervalue){
        this.Number_of_Orders__c = totalOrdervalue;
    }
    public double getNumber_of_Orders__c(){
        return this.Number_of_Orders__c;
    }
    @Override
	public String toString() {
		return " {Customer_Code__c=" + Customer_Code__c + ", Last_Order_Date__c=" + Last_Order_Date__c
				+ ", Number_of_Orders__c=" + Number_of_Orders__c + ", LifeTime_Value__c=" + LifeTime_Value__c + "}";
	}
	public void setLifeTime_Value__c(double LifeTime_Value__c){
        this.LifeTime_Value__c = LifeTime_Value__c;
    }
    public double getLifeTime_Value__c(){
        return this.LifeTime_Value__c;
    }
}