package com.TEAF.childstepDefinition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONValue;

public class MissingCustomerUpsert {
	
	private String AccountId;
	private String LastName;
	private String FirstName;
	private String RecordTypeId;
	private String OtherStreet;
	private String OtherCity;
	private String OtherState;
	private String OtherPostalCode;
	private String OtherCountry;
	private String HomePhone;
	private String MobilePhone;
	private String Email;
	private Boolean HasOptedOutOfEmail;
	private String Customer_Code__c;
	private String Invalid_email_address__c;
	
	
	public String getAccountId() {
		return AccountId;
	}
	public void setAccountId(String accountId) {
		AccountId = accountId;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getRecordTypeId() {
		return RecordTypeId;
	}
	public void setRecordTypeId(String recordTypeId) {
		RecordTypeId = recordTypeId;
	}
	public String getOtherStreet() {
		return OtherStreet;
	}
	public void setOtherStreet(String otherStreet) {
		OtherStreet = otherStreet;
	}
	public String getOtherCity() {
		return OtherCity;
	}
	public void setOtherCity(String otherCity) {
		OtherCity = otherCity;
	}
	public String getOtherState() {
		return OtherState;
	}
	public void setOtherState(String otherState) {
		OtherState = otherState;
	}
	public String getOtherPostalCode() {
		return OtherPostalCode;
	}
	public void setOtherPostalCode(String otherPostalCode) {
		OtherPostalCode = otherPostalCode;
	}
	public String getOtherCountry() {
		return OtherCountry;
	}
	public void setOtherCountry(String otherCountry) {
		OtherCountry = otherCountry;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getMobilePhone() {
		return MobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		MobilePhone = mobilePhone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public Boolean getHasOptedOutOfEmail() {
		return HasOptedOutOfEmail;
	}
	public void setHasOptedOutOfEmail(Boolean hasOptedOutOfEmail) {
		HasOptedOutOfEmail=hasOptedOutOfEmail;
	}
	public String getCustomer_Code__c() {
		return Customer_Code__c;
	}
	public void setCustomer_Code__c(String customer_Code__c) {
		Customer_Code__c = customer_Code__c;
	}
	public String getInvalid_email_address__c() {
		return Invalid_email_address__c;
	}
	public void setInvalid_email_address__c(String invalid_email_address__c) {
		Invalid_email_address__c = invalid_email_address__c;
	}
}