package com.TEAF.childstepDefinition;

import java.util.ArrayList;
import java.util.List;

public class SFDCpayloadlist 
{
	List<SfdcRequestPayload> Requestpl = new ArrayList<SfdcRequestPayload>();

	public List<SfdcRequestPayload> getRequestpl() {
		return Requestpl;
	}

	public void setRequestpl(List<SfdcRequestPayload> requestpl) {
		Requestpl = requestpl;
	}
	
}